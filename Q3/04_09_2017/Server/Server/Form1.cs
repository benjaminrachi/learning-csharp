﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using Common;

namespace Server
{
    public partial class Form1 : Form
    {
        public static Random ServerRand = new Random();
        public static Color ServerColor ;

        public Form1()
        {
            InitializeComponent();
            HttpChannel chnl = new HttpChannel(1234);
            ChannelServices.RegisterChannel(chnl, false);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(ServerPartFactory),
                "_Server_",
                WellKnownObjectMode.Singleton);

            switch (ServerRand.Next(3))
            {
                case 0: ServerColor = this.BackColor = Color.Red; return;
                case 1: ServerColor = this.BackColor = Color.Green; return;
                case 2: ServerColor = this.BackColor = Color.Blue; return;
            }

            Label label = new Label();
            label.Text = "Server is running...";
            label.BackColor = Color.BurlyWood;
            label.ForeColor = Color.Black;
            label.Name = "Arial";
            Controls.Add(label);
        }
    }

    class ServerPart : MarshalByRefObject, ICommon
    {
        private SortedList buttons = new SortedList();
        private SortedList labels = new SortedList();

        private Random random = new Random();

        public int[] addControl(MControl c)
        {
            if (c.backColor.R != 0 && Form1.ServerColor.R != 0)
            {
                if (c.className == "Button") buttons.Add(c.backColor.R + random.Next(1000) * 0.00001, c.index);
                else labels.Add(c.backColor.R + random.Next(1000) * 0.00001, c.index);

                return refresh();
            }
            else if (c.backColor.G != 0 && Form1.ServerColor.G != 0)
            {
                if (c.className == "Button") buttons.Add(c.backColor.G + random.Next(1000) * 0.00001, c.index);
                else labels.Add(c.backColor.G + random.Next(1000) * 0.00001, c.index);

                return refresh();
            }
            else if (c.backColor.B != 0 && Form1.ServerColor.B != 0)
            {
                if (c.className == "Button") buttons.Add(c.backColor.B + random.Next(1000) * 0.00001, c.index);
                else labels.Add(c.backColor.B + random.Next(1000) * 0.00001, c.index);

                return refresh();
            }

            return null;

        }

        public int[] refresh()
        {
            int[] returnArr = new int[buttons.Count + labels.Count];
            int k = 0;
            for (int i = 0; i < buttons.Count; i++)
                returnArr[k++] = (int)(buttons.GetByIndex(i));
            for (int i = 0; i < labels.Count; i++)
                returnArr[k++] = (int)(labels.GetByIndex(i));

            return returnArr;
        }

    }
    class ServerPartFactory : MarshalByRefObject, ICommonFactory
    {
        public ServerPartFactory()
        {
        }
        public ICommon getNewInstance()
        {
            return new ServerPart();
        }
    }
}