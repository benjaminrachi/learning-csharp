﻿using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Common
{
    public interface ICommon
    {
        int[] addControl(MControl mcontrol);
        int[] refresh();


    }

    public interface ICommonFactory
    {
        ICommon getNewInstance();
    }

    [Serializable]
    public class MControl
    {
        public string className;
        public Size size;
        public Color backColor;
        public int index;

        public MControl(string className, Size size, Color backColor, int index)
        {
            this.backColor = backColor;
            this.className = className;
            this.size = size;
            this.index = index;
        }
    }
}

