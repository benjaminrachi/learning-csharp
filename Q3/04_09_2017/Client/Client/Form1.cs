﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using Common;
using System.Runtime.Remoting;
using System.ComponentModel;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Client
{
    public partial class Form1 : Form
    {
        private ICommon myServer;
        private Random myRand = new Random();

        private Control[] arrControls;
        private Control[] arrResults;
        int currPosition = 3;
        public Form1()
        {
            InitializeComponent();

            HttpChannel channel = new HttpChannel();
            ChannelServices.RegisterChannel(channel, false);

            ICommonFactory myICommonFactory = (ICommonFactory)Activator.GetObject( typeof(ICommonFactory), "http://localhost:1234/_Server_");

            myServer = myICommonFactory.getNewInstance();

            arrControls = new Control[25];
            
            for (int i = 0; i < arrControls.Length; i++)
            {
                if( myRand.Next(2) == 0) arrControls[i] = new Button();
                else arrControls[i] = new Label();
                    
                switch (myRand.Next(3))
                {
                    case 0: arrControls[i].BackColor = Color.FromArgb(myRand.Next(130, 256), 0, 0); break;
                    case 1: arrControls[i].BackColor = Color.FromArgb(0, myRand.Next(130, 256), 0); break;
                    case 2: arrControls[i].BackColor = Color.FromArgb(0, 0, myRand.Next(130, 256)); break;
                }

                arrControls[i].TabIndex = i;
                arrControls[i].Size = new Size(40, 40);
                arrControls[i].Location = new Point(currPosition, 3);
                currPosition += arrControls[i].Size.Width + 2;
                arrControls[i].Click += new EventHandler(allControls_Click);
                this.Controls.Add(arrControls[i]);
            }
        }

        private void allControls_Click(object sender, EventArgs e)
        {
            Control c = (Control) sender;
            MControl m = new MControl(c.GetType().Name, c.Size, c.BackColor, c.TabIndex);
            var result = myServer.addControl(m);
            if (result == null) return;
            PaintDestinationAndRemoveSource(result);

            //2nd option
//            arrResults = new Control[result.Length];
//            currPosition = 3;
//            foreach (var item in result)
//            {
//                arrControls[item].Location = new Point(currPosition, 50);
//                currPosition += 42;
//            }

            
        }

        private void PaintDestinationAndRemoveSource(int[] sortedList)
        {
            // skip if no changes in model
            if (sortedList == null) return;

            // clean
            RemoveAllItemsInResult();

            // offset for paint
            int paintOffset = 0;

            // repaint state
            int itemsAdded = 0;

            arrResults = new Control[sortedList.Length];

            foreach (var item in sortedList)
            {
                //arrControls[item].Location = new Point(paintOffset, 50);
                Control control = arrControls[item];

                if (control == null) continue;

                if (control.GetType().Name.Equals("Button"))
                    arrResults[itemsAdded] = new Button();
                else
                    arrResults[itemsAdded] = new Label();

                arrResults[itemsAdded].BackColor = control.BackColor;
                arrResults[itemsAdded].Size = control.Size;
                arrResults[itemsAdded].Location = new Point(paintOffset, 50);

                paintOffset += arrResults[itemsAdded].Size.Width + 2;

                this.Controls.Add(arrResults[itemsAdded]);

                Controls.Remove(arrControls[item]);
                itemsAdded++;

            }
        }

        private void RemoveAllItemsInResult()
        {
            if (arrResults == null) return;

            for (int i = 0; i < arrResults.Length; i++)
            {
                if (arrResults[i] == null) continue;

                Controls.Remove(arrResults[i]);

                arrResults[i] = null;
            }

            arrResults = null;
        }
    }
}

