﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public interface ICommon
    {
        void addControls(MControl[] controls);
        SortedList refresh(int clientChoice, int itemCount);
    }
    public interface ICommonFactory
    {
        ICommon getNewInstance();
    }

    [Serializable]
    public class MControl
    {
        public string className;
        public Size size;
        public Color backColor;

        public MControl(string className, Size size, Color backColor)
        {
            this.backColor = backColor;
            this.className = className;
            this.size = size;
        }
    }
}
