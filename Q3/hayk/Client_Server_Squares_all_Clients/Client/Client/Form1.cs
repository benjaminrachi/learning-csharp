﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using Common;

namespace Client
{
    public partial class Form1 : Form
    {
        private Control[] arrControls;
        private ICommon myICommon;

        private Control[] arrControls_Squares_All_Clients;

        public Form1()
        {
            InitializeComponent();

            Random myRand = new Random();
            int arrSize = myRand.Next(5, 10);

            int currPosition = 2;
            arrControls = new Control[arrSize];

            Color myColor = Color.FromArgb(myRand.Next(100, 256), myRand.Next(100, 256), myRand.Next(100, 256));
            for (int i = 0; i < arrSize; i++)
            {
                if(myRand.Next(2) == 0)
                    arrControls[i] = new Button();
                else
                    arrControls[i] = new Label();
                arrControls[i].BackColor = myColor;

                int tempXY = myRand.Next(20, 40);
                switch (myRand.Next(3))
                {
                    case 0: arrControls[i].Size = new Size(tempXY, tempXY);     break;
                    case 1: arrControls[i].Size = new Size(tempXY * 2, tempXY); break;
                    case 2: arrControls[i].Size = new Size(tempXY, tempXY * 2); break;
                }

                arrControls[i].Location = new Point(currPosition, 3);
                currPosition += arrControls[i].Size.Width + 2;
                this.Controls.Add(arrControls[i]);
            }

            HttpChannel channel = new HttpChannel();
            ChannelServices.RegisterChannel(channel, false);

            myICommon = (ICommon)Activator.GetObject(
                typeof(ICommon),
                "http://localhost:1234/_Server_");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MControl[] cons = arrControls
                    .Select(c => new MControl(c.GetType().Name, c.Size, c.BackColor))
                    .ToArray();

            myICommon.addControls(cons);
        }

        private int getItemCount()
        {
            return arrControls_Squares_All_Clients == null ? 0 : arrControls_Squares_All_Clients.Length;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            paintSortedList(myICommon.refresh(getItemCount()));
        }

        private void paintSortedList(MControl[] items)
        {
            // skip if no changes in model
            if (items == null) return;

            // clean
            removeAllItemsInResult();

            // offset for paint
            int paintOffset = 0;

            // repaint state
            int itemsAdded = 0;

            arrControls_Squares_All_Clients = new Control[items.Length];

            foreach (MControl control in items)
            {

                if (control.className.Equals("Button"))
                    arrControls_Squares_All_Clients[itemsAdded] = new Button();
                else
                    arrControls_Squares_All_Clients[itemsAdded] = new Label();

                arrControls_Squares_All_Clients[itemsAdded].BackColor = control.backColor;
                arrControls_Squares_All_Clients[itemsAdded].Size = control.size;
                arrControls_Squares_All_Clients[itemsAdded].Location = new Point(paintOffset, 100);

                paintOffset += arrControls_Squares_All_Clients[itemsAdded].Size.Width + 2;

                this.Controls.Add(arrControls_Squares_All_Clients[itemsAdded]);

                itemsAdded++;
            }
        }

        private void removeAllItemsInResult()
        {
            if (arrControls_Squares_All_Clients == null) return;

            for (int i = 0; i < arrControls_Squares_All_Clients.Length; i++)
            {
                if (arrControls_Squares_All_Clients[i] == null) continue;

                Controls.Remove(arrControls_Squares_All_Clients[i]);

                arrControls_Squares_All_Clients[i] = null;
            }

            arrControls_Squares_All_Clients = null;
        }
    }
}

