using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using System.Collections;
using Common;
namespace Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            HttpChannel chnl = new HttpChannel(1234);
            ChannelServices.RegisterChannel(chnl, false);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(ServerPart),
                "_Server_",
                WellKnownObjectMode.Singleton);
        }
    }

    class ServerPart : MarshalByRefObject, ICommon
    {
        private List<MControl> squares = new List<MControl>();

        private Random rand = new Random();

        public void addControls(MControl[] controls)
        {
            foreach (var c in controls)
            {
                if (isSquare(c.size) && !isControlFoundInArray(c))
                    squares.Add(c);
            }
        }

        private bool isControlFoundInArray(MControl c)
        {
            if (squares == null) return false;

            var found = squares.FindAll(control =>
                    control.className == c.className &&
                    control.size.Height == c.size.Height &&
                    control.size.Width == c.size.Width);

            return found.Count > 0;
        }

        public MControl[] refresh(int itemCount)
        {   
           return squares.Count == itemCount ? null : squares.ToArray();
        }

        private bool isSquare(Size size) { return size.Height == size.Width; }
    }
}