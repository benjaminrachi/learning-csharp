using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using System.Collections;
using System.Linq;
using Common;

namespace Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            HttpChannel chnl = new HttpChannel(1234);
            ChannelServices.RegisterChannel(chnl, false);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(ServerPart),
                "_Server_",
                WellKnownObjectMode.Singleton);

            Label label = new Label();
            label.Text = "Server is running...";
            label.ForeColor = Color.Black;
            label.Name = "Arial";
            Controls.Add(label);
        }
    }

    class ServerPart : MarshalByRefObject, ICommon
    {
        int nextFreeId=1;
        private SortedList squares = new SortedList();
        public void addControls(MControl[] controls)
        {
            foreach (var c in controls)
            {
                if (c.size.Height == c.size.Width && !squares.ContainsKey(c.size.Width * c.size.Height))
                {
                    squares.Add(c.size.Width*c.size.Height, c);
                }
                    
            }
            
        }

        public MControl[] refresh(int itemCount)
        {
            MControl[] resultArray = new MControl[squares.Count];
            squares.Values.CopyTo(resultArray, 0);
            
            return resultArray.OrderBy(x => x.clientId).ThenBy(x => x.size.Width* x.size.Height).ToArray();
        }

        public int getClientId()
        {
            return nextFreeId++;
        }
    }
}