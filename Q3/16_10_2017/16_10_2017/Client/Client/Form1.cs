﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using Common;

namespace Client
{
    public partial class Form1 : Form
    {
        private Control[] arrControls;
        private ICommon myICommon;
        private int id;
        Random myRand = new Random();

        private Control[] arrControls_result;

        public Form1()
        {
            InitializeComponent();

            HttpChannel channel = new HttpChannel();
            ChannelServices.RegisterChannel(channel, false);

            myICommon = (ICommon)Activator.GetObject(
                typeof(ICommon),
                "http://localhost:1234/_Server_");

            id = myICommon.getClientId();

            int arrSize = myRand.Next(5, 10);

            int currPosition = 2;
            arrControls = new Control[arrSize];

            Color myColor = Color.FromArgb(myRand.Next(100, 256), myRand.Next(100, 256), myRand.Next(100, 256));
            for (int i = 0; i < arrSize; i++)
            {
                if(myRand.Next(2) == 0)
                    arrControls[i] = new Button();
                else
                    arrControls[i] = new Label();
                arrControls[i].BackColor = myColor;

                int tempXY = myRand.Next(20, 40);
                switch (myRand.Next(3))
                {
                    case 0: arrControls[i].Size = new Size(tempXY, tempXY);     break;
                    case 1: arrControls[i].Size = new Size(tempXY * 2, tempXY); break;
                    case 2: arrControls[i].Size = new Size(tempXY, tempXY * 2); break;
                }

                arrControls[i].Location = new Point(currPosition, 3);
                currPosition += arrControls[i].Size.Width + 2;
                this.Controls.Add(arrControls[i]);
            }

            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //copiying the arrControls with all the properties and sending to the server as serializable object.
            MControl[] cons = arrControls
                .Select(c => new MControl(c.GetType().Name, c.Size, c.BackColor, this.id))
                .ToArray();

            myICommon.addControls(cons);


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            PaintSortedList(myICommon.refresh( GetItemCount()));
        }

        private int GetItemCount()
        {
            return arrControls_result?.Length ?? 0;
        }


        private void PaintSortedList(MControl[] sortedList)
        {
            // skip if no changes in model
            if (sortedList == null) return;

            // clean
            RemoveAllItemsInResult();

            // offset for paint
            int paintOffset = 0;

            // repaint state
            int itemsAdded = 0;

            arrControls_result = new Control[sortedList.Length];
            
            foreach (var control in sortedList)
            {

                if (control.className.Equals("Button"))
                    arrControls_result[itemsAdded] = new Button();
                else
                    arrControls_result[itemsAdded] = new Label();

                arrControls_result[itemsAdded].BackColor = control.backColor;
                arrControls_result[itemsAdded].Size = control.size;
                arrControls_result[itemsAdded].Location = new Point(paintOffset, 100);
                arrControls_result[itemsAdded].Text = control.clientId.ToString();

                paintOffset += arrControls_result[itemsAdded].Size.Width + 2;

                this.Controls.Add(arrControls_result[itemsAdded]);

                itemsAdded++;

            }
        }

        private void RemoveAllItemsInResult()
        {
            if (arrControls_result == null) return;

            for (int i = 0; i < arrControls_result.Length; i++)
            {
                if (arrControls_result[i] == null) continue;

                Controls.Remove(arrControls_result[i]);

                arrControls_result[i] = null;
            }

            arrControls_result = null;
        }
    }
}

