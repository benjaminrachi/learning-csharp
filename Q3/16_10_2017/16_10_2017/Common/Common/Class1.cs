using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Messaging;
using System.Drawing;
using System.Collections;

namespace Common
{
    
    public interface ICommon
    {
        void addControls(MControl[] controls);
        MControl[] refresh( int itemCount);
        int getClientId();
    }

    [Serializable]
    public class MControl
    {
        public string className;
        public Size size;
        public Color backColor;
        public int clientId;

        public MControl(string className, Size size, Color backColor, int id)
        {
            this.backColor = backColor;
            this.className = className;
            this.size = size;
            this.clientId = id;
        }
    }
}
