using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using System.Collections;
using Common;
namespace Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            HttpChannel chnl = new HttpChannel(1234);
            ChannelServices.RegisterChannel(chnl, false);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(ServerPart),
                "_Server_",
                WellKnownObjectMode.Singleton);

            Label label = new Label();
            label.Text = "Server is running...";
            label.ForeColor = Color.Black;
            label.Name = "Arial";
            Controls.Add(label);
        }
    }

    class ServerPart : MarshalByRefObject, ICommon
    {
        private SortedList sqares = new SortedList();
        private SortedList rectangles = new SortedList();

        private Random random = new Random();

        public void addControls(MControl[] controls)
        {
            foreach (var c in controls)
            {
                if (c.size.Height != c.size.Width)
                    rectangles.Add( (c.size.Height*c.size.Width) + (random.Next(1000) * 0.00001) , c);
                else 
                    sqares.Add((c.size.Height * c.size.Width) + (random.Next(1000) * 0.00001), c);
            }
        }

        public SortedList refresh(int clientChoice, int itemCount)
        {
            switch (clientChoice)
            {
                case 0: return sqares.Count == itemCount ? null : sqares;

                case 1: return rectangles.Count == itemCount ? null : rectangles;

                default: return null;
            }
        }
    }
}