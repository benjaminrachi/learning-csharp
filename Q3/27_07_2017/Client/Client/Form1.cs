﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using Common;

namespace Client
{
    public partial class Form1 : Form
    {
        private Control[] arrControls;
        private ICommon myICommon;
        private Control[] arrControls_result;
        public FlowLayoutPanel pnl;
        Random myRand = new Random();

        public Form1()
        {
            InitializeComponent();

            #region arrControls

            int arrSize = myRand.Next(5, 10);

            int currPosition = 2;
            arrControls = new Control[arrSize];

            Color myColor = Color.FromArgb(myRand.Next(100, 256), myRand.Next(100, 256), myRand.Next(100, 256));
            for (int i = 0; i < arrSize; i++)
            {
                if(myRand.Next(2) == 0)
                    arrControls[i] = new Button();
                else
                    arrControls[i] = new Label();
                arrControls[i].BackColor = myColor;

                int tempXY = myRand.Next(20, 40);
                switch (myRand.Next(4))
                {
                    case 0:
                    case 1: arrControls[i].Size = new Size(tempXY, tempXY); break;
                    case 2: arrControls[i].Size = new Size(tempXY * 2, tempXY); break;
                    case 3: arrControls[i].Size = new Size(tempXY, tempXY * 2); break;
                }

                arrControls[i].Location = new Point(currPosition, 3);
                currPosition += arrControls[i].Size.Width + 2;
                this.Controls.Add(arrControls[i]);
            }

            #endregion

            #region timer

            Timer timer = new Timer();
            timer.Enabled = true;
            timer.Interval = 10000;
            timer.Tick += timer_Tick;

            #endregion

            #region RadioButtons

            pnl = new FlowLayoutPanel();
            pnl.FlowDirection = FlowDirection.TopDown;
            pnl.AutoSize = true;
            //pnl.BackColor = Color.Bisque;
            pnl.Location = new Point(2 + arrSize * 80 + 10, 3);
            pnl.Size = new Size(200, 40);


            //Each radiobutton has an index 
            RadioButton red = new RadioButton();
            red.Text = "all squares";
            red.AutoSize = true;
            red.TabIndex = 0;
            red.Checked = true;
            red.CheckedChanged += radioButton_CheckedChanged;
            pnl.Controls.Add(red);

            RadioButton green = new RadioButton();
            green.Text = "all rectangles";
            green.AutoSize = true;
            green.TabIndex = 1;
            green.Checked = false;
            green.CheckedChanged += radioButton_CheckedChanged;
            pnl.Controls.Add(green);


            this.Controls.Add(pnl);

            #endregion

            #region connecting to the server
            
                        HttpChannel channel = new HttpChannel();
                        ChannelServices.RegisterChannel(channel, false);
            
                        myICommon = (ICommon)Activator.GetObject(
                            typeof(ICommon),
                            "http://localhost:1234/_Server_");

            #endregion
            this.Size = new Size(pnl.Right + 10, 220);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //copiying the arrControls with all the properties and sending to the server as serializable object. 
            MControl[] cons = arrControls
                .Select(c => new MControl(c.GetType().Name, c.Size, c.BackColor))
                .ToArray();

            myICommon.addControls(cons);
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbSelected = pnl.Controls
                .OfType<RadioButton>()
                .FirstOrDefault(r => r.Checked);
            Console.WriteLine(rbSelected?.Text);
            PaintSortedList(myICommon.refresh(rbSelected?.TabIndex ?? 0, GetItemCount()));
        }

        private int GetItemCount()
        {
            return arrControls_result?.Length ?? 0;
        }

        private void PaintSortedList(SortedList sortedList)
        {
            // skip if no changes in model
            if (sortedList == null) return;

            // clean
            RemoveAllItemsInResult();

            // offset for paint
            int paintOffset = 0;

            // repaint state
            int itemsAdded = 0;

            arrControls_result = new Control[sortedList.Count];

            foreach (DictionaryEntry item in sortedList)
            {
                MControl control = (MControl)item.Value;

                if (control.className.Equals("Button"))
                    arrControls_result[itemsAdded] = new Button();
                else
                    arrControls_result[itemsAdded] = new Label();

                arrControls_result[itemsAdded].BackColor = control.backColor;
                arrControls_result[itemsAdded].Size = control.size;
                arrControls_result[itemsAdded].Location = new Point(paintOffset, 100);

                paintOffset += arrControls_result[itemsAdded].Size.Width + 2;

                this.Controls.Add(arrControls_result[itemsAdded]);

                itemsAdded++;

            }
        }

        private void RemoveAllItemsInResult()
        {
            if (arrControls_result == null) return;

            for (int i = 0; i < arrControls_result.Length; i++)
            {
                if (arrControls_result[i] == null) continue;

                Controls.Remove(arrControls_result[i]);

                arrControls_result[i] = null;
            }

            arrControls_result = null;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            radioButton_CheckedChanged(null, null);
        }
    }
}

