using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using System.Collections;
using Common;
namespace Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            HttpChannel chnl = new HttpChannel(1234);
            ChannelServices.RegisterChannel(chnl, false);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(ServerPartFactory),
                "_Server_",
                WellKnownObjectMode.Singleton);

            Label label = new Label();
            label.Text = "Server is running...";
            label.ForeColor = Color.Black;
            label.Name = "Arial";
            Controls.Add(label);

        }
    }


    class ServerPart : MarshalByRefObject, ICommon
    {
        private SortedList Red = new SortedList();
        private SortedList Green = new SortedList();
        private SortedList Blue = new SortedList();

        private Random random = new Random();

        public void addControls(MControl[] controls)
        {
            foreach (var c in controls)
            {
                if (c.backColor.R != 0)
                    Red.Add(c.backColor.R + random.Next(1000) * 0.00001, c);
                else if (c.backColor.G != 0)
                    Green.Add(c.backColor.G + random.Next(1000) * 0.00001, c);
                else if (c.backColor.B != 0)
                    Blue.Add(c.backColor.B + random.Next(1000) * 0.00001, c);
            }
        }

        public SortedList refresh(int previousChoice, int nextChoice, int itemCount)
        {
            switch (nextChoice)
            {
                case 0: return Red.Count == itemCount && previousChoice==nextChoice? null : Red;

                case 1: return Green.Count == itemCount && previousChoice == nextChoice? null : Green;

                case 2: return Blue.Count == itemCount && previousChoice == nextChoice? null : Blue;

                default: return null;
            }
        }

    }

    class ServerPartFactory : MarshalByRefObject, ICommonFactory
    {
        public ServerPartFactory()
        {
        }
        public ICommon getNewInstance()
        {
            return new ServerPart();
        }
    }
}