﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using Common;

namespace Client
{
    public partial class Form1 : Form
    {
        private Control[] arrControls;
        int currPosition = 2;
        Size arrControlSize = new Size(30, 30);

        public FlowLayoutPanel pnl;

        private Control[] arrControls_result;

        private ICommon serverInstance; //server
        public Random random = new Random();

        private int previousSelection = -1;


        public Form1()
        {
            InitializeComponent();

            #region form
            switch (random.Next(2))
            {
                case 0: this.Text = "Button"; break;
                case 1: this.Text = "Label"; break;
            }

            int arrSize = random.Next(5, 10);
            arrControls = new Control[arrSize];

            #endregion

            #region timer

            Timer timer = new Timer();
            timer.Enabled = true;
            timer.Interval = 10000;
            timer.Tick += timer_Tick;

            #endregion

            #region RadioButtons

            pnl = new FlowLayoutPanel();
            pnl.FlowDirection = FlowDirection.LeftToRight;
            pnl.AutoSize = true;
            //pnl.BackColor = Color.Bisque;
            pnl.Location = new Point(2 + arrSize * arrControlSize.Width + 50, 3);
            pnl.Size = new Size(50, 30);


            //Each radiobutton has an index 
            RadioButton red = new RadioButton();
            red.Text = "red";
            red.AutoSize = true;
            red.TabIndex = 0;
            red.Checked = true;
            red.CheckedChanged += radioButton_CheckedChanged;
            pnl.Controls.Add(red);

            RadioButton green = new RadioButton();
            green.Text = "green";
            green.AutoSize = true;
            green.TabIndex = 1;
            green.Checked = false;
            green.CheckedChanged += radioButton_CheckedChanged;
            pnl.Controls.Add(green);


            RadioButton blue = new RadioButton();
            blue.Text = "blue";
            blue.AutoSize = true;
            blue.TabIndex = 2;
            blue.Checked = false;
            blue.CheckedChanged += radioButton_CheckedChanged;
            pnl.Controls.Add(blue);

            this.Controls.Add(pnl);

            #endregion

            #region arrControls
            for (int i = 0; i < arrSize; i++)
            {
                switch (random.Next(2)) //type
                {
                    case 0: arrControls[i] = new Button(); break;
                    case 1: arrControls[i] = new Label(); break;
                }

                switch (random.Next(3)) //color
                {
                    case 0: arrControls[i].BackColor = Color.FromArgb(random.Next(100, 256), 0, 0); break; //red
                    case 1: arrControls[i].BackColor = Color.FromArgb(0, random.Next(100, 256), 0); break; //green
                    case 2: arrControls[i].BackColor = Color.FromArgb(0, 0, random.Next(100, 256)); break; //blue
                }
                arrControls[i].Size = arrControlSize;
                arrControls[i].Location = new Point(currPosition, 3);
                currPosition += arrControlSize.Width + 2;
                this.Controls.Add(arrControls[i]);
            }
            #endregion

            #region connect to the server

            HttpChannel channel = new HttpChannel();
            ChannelServices.RegisterChannel(channel, false);

            ICommonFactory myICommonFactory = (ICommonFactory)Activator.GetObject(typeof(ICommonFactory), "http://localhost:1234/_Server_");

            serverInstance = myICommonFactory.getNewInstance();
            #endregion

            #region outputs
            Console.WriteLine("pnl width =" + pnl.Size.Width + ", pnl height =" + pnl.Size.Height);
            Console.WriteLine("pnl left=" + pnl.Left);
            Console.WriteLine("pnl right=" + pnl.Right);
            Console.WriteLine("form width =" + this.Size.Width + ", form height =" + this.Size.Height);
            #endregion

            this.Size = new Size(pnl.Right + 2, 200);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //copiying the arrControls with all the properties and sending to the server as serializable object.
            MControl[] cons = arrControls
                .Select(c => new MControl(c.GetType().Name, c.Size, c.BackColor))
                .ToArray();

            serverInstance.addControls(cons);
        }


        private void timer_Tick(object sender, EventArgs e)
        {
            Console.WriteLine("tick called!");
//            radioButton_CheckedChanged(null, null);

            RadioButton rbSelected = pnl.Controls
                .OfType<RadioButton>()
                .FirstOrDefault(r => r.Checked);
            Console.WriteLine(rbSelected?.Text);
            PaintSortedList(serverInstance.refresh(previousSelection, rbSelected?.TabIndex ?? 0, GetItemCount()));
            previousSelection = rbSelected.TabIndex;
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbSelected = pnl.Controls
                .OfType<RadioButton>()
                .FirstOrDefault(r => r.Checked);
            Console.WriteLine(rbSelected?.Text);
            PaintSortedList(serverInstance.refresh(previousSelection, rbSelected?.TabIndex ?? 0, GetItemCount()));
            previousSelection = rbSelected.TabIndex;
        }

        private int GetItemCount()
        {
            return arrControls_result?.Length ?? 0;
        }


        private void PaintSortedList(SortedList sortedList)
        {
            // skip if no changes in model
            if (sortedList == null) return;

            // clean
            RemoveAllItemsInResult();

            // offset for paint
            int paintOffset = 0;

            // repaint state
            int itemsAdded = 0;

            arrControls_result = new Control[sortedList.Count];

            foreach (DictionaryEntry item in sortedList)
            {
                MControl control = (MControl)item.Value;

                if (control.className.Equals("Button"))
                    arrControls_result[itemsAdded] = new Button();
                else
                    arrControls_result[itemsAdded] = new Label();

                arrControls_result[itemsAdded].BackColor = control.backColor;
                arrControls_result[itemsAdded].Size = control.size;
                arrControls_result[itemsAdded].Location = new Point(paintOffset, 100);

                paintOffset += arrControls_result[itemsAdded].Size.Width + 2;

                this.Controls.Add(arrControls_result[itemsAdded]);

                itemsAdded++;

            }
        }

        private void RemoveAllItemsInResult()
        {
            if (arrControls_result == null) return;

            for (int i = 0; i < arrControls_result.Length; i++)
            {
                if (arrControls_result[i] == null) continue;

                Controls.Remove(arrControls_result[i]);

                arrControls_result[i] = null;
            }

            arrControls_result = null;
        }


    }
}

