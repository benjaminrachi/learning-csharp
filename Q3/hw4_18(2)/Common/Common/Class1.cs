using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Messaging;
using System.Drawing;
using System.Collections;

namespace Common
{

    public interface ICommon
    {
        void addControls(MControl[] controls);
        SortedList refresh(int previousChoice, int nextChoice, int itemCount);
    }
    public interface ICommonFactory
    {
        ICommon getNewInstance();
    }

    [Serializable]
    public class MControl
    {
        public string className;
        public Size size;
        public Color backColor;

        public MControl(string className, Size size, Color backColor)
        {
            this.backColor = backColor;
            this.className = className;
            this.size = size;
        }
    }
}
