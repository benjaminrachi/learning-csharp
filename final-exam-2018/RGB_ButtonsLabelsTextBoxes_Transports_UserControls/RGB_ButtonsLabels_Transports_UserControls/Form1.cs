﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Threading;

namespace RGB_ButtonsLabels_Transports_UserControls
{
    public partial class Form1 : Form
    {
        private UserControl1 UC_From;
        private UserControl1[] arrUC_Transport = new UserControl1[3], arrUC_To = new UserControl1[3];
        private const int N_rows_From = 3, N_columns_From = 30, N_To = 40,  N_Transport = 5;

        public delegate void myDelegate(int i, int index);

        private int[] arrCounter_Transport = new int[3];
        private int[] position_Transport = { 1, 1, 1 };
        private int[] position_Destination = { 1, 1, 1 };
        private int[] arrCounter_Destination = { 0, 0, 0 }; //[counterButton, counterLabel, counterTextBox]

        private string[] arrColors = { "Red", "Green", "Blue" };
        private string[] arrTypes = { "Button", "Label", "TextBox" };
        private int[] arrIndex_Color = { 0, 0, 0 }; // 0-Red, 1-Blue, 2-Green, 3-end
        //private int[] arrColor_Flag = new int[3];  // 0-Red, 1-Blue, 2-Green, 3-end
        private bool[] arrColorIsEnd = { false, false, false };


        private Thread[] Thread_toTransport = new Thread[3],
                         Thread_toDestination = new Thread[3];

        private AutoResetEvent[] Reset_toTransport = new AutoResetEvent[3],
                                 Reset_toDestination = new AutoResetEvent[3];


        public Form1()
        {
            InitializeComponent();

            UC_From = new UserControl1(N_rows_From, N_columns_From, "Full");
            UC_From.Location = new Point(2, 47);
            this.Controls.Add(UC_From);

            for (int i = 0; i < 3; i++)
            {
                arrUC_Transport[i] = new UserControl1(1, N_Transport, "Empty");
                arrUC_Transport[i].Location = new Point(2 + 250 * i, 182);
                this.Controls.Add(arrUC_Transport[i]);
            }

            for (int i = 0; i < 3; i++)
            {
                arrUC_To[i] = new UserControl1(1, N_To, "Empty");
                arrUC_To[i].Location = new Point(2, 250 + 73 * i);
                this.Controls.Add(arrUC_To[i]);
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 3; i++)
            {
                Thread_toTransport[i] = new Thread(function_toTransport);
                Thread_toDestination[i] = new Thread(function_toDestination);

                Reset_toTransport[i] = new AutoResetEvent(false);
                Reset_toDestination[i] = new AutoResetEvent(false);

                Thread_toTransport[i].Start(i); //get the index of a type: button, label, textbox
                Thread_toDestination[i].Start(i); //get the index of a type: button, label, textbox
            }
        }


        #region transport
        private void function_toTransport(object obj)
        {
            int indexType = (int)obj;

            switch (indexType)
            {
                case 0: //Button
                    function_toTransport_Button(indexType);
                    break;

                case 1: //Label
                    function_toTransport_Label(indexType);
                    break;

                case 2: //TextBox
                    function_toTransport_TextBox(indexType);
                    break;
            }
        }

        private void function_toTransport_Button(int indexType)
        {
            int i = 0;
            while (true)
            {
                //int maxShade = 257;
                int minIndex = -1;
                for (i = 0; i < N_columns_From* N_rows_From; i++)
                {
                    if (UC_From.arrControls[i] == null)
                        continue;
                    Control temp = UC_From.arrControls[i];
                    if (isValidType(temp, indexType) )
                    {
                        minIndex = i;
                        //maxShade = temp.BackColor.R;
                    }
                }

                if (minIndex == -1)
                {
                    arrIndex_Color[indexType]++;
                    if (arrIndex_Color[indexType] == 1 || arrIndex_Color[indexType] == 2) //fill the transport with the other colors
                        continue;
                    else
                    {
                        arrColorIsEnd[indexType] = true;
                        Reset_toDestination[indexType].Set();
                        break;
                    }
                }

                if (arrCounter_Transport[indexType] < N_Transport)
                {
                    Invoke(new myDelegate(toTransport), minIndex, indexType);
                    Thread.Sleep(100);
                    arrCounter_Transport[indexType]++;
                }
                else
                {
                    Reset_toDestination[indexType].Set();
                    Reset_toTransport[indexType].WaitOne();
                }

            }
            arrColorIsEnd[indexType] = true;
            Reset_toDestination[indexType].Set();
        }

        private void function_toTransport_Label(int indexType)
        {
            int i = 0;
            while (true)
            {
                //int maxShade = 257;
                int minIndex = -1;
                for (i = 0; i < N_columns_From * N_rows_From; i++)
                {
                    if (UC_From.arrControls[i] == null)
                        continue;
                    Control temp = UC_From.arrControls[i];
                    if (isValidType(temp, indexType))
                    {
                        minIndex = i;
                        //maxShade = temp.BackColor.R;
                    }
                }

                if (minIndex == -1)
                {
                    arrIndex_Color[indexType]++;
                    if (arrIndex_Color[indexType] == 1 || arrIndex_Color[indexType] == 2) //fill the transport with the other colors
                        continue;
                    else
                    {
                        arrColorIsEnd[indexType] = true;
                        Reset_toDestination[indexType].Set();
                        break;
                    }
                }

                if (arrCounter_Transport[indexType] < N_Transport)
                {
                    Invoke(new myDelegate(toTransport), minIndex, indexType);
                    Thread.Sleep(100);
                    arrCounter_Transport[indexType]++;
                }
                else
                {
                    Reset_toDestination[indexType].Set();
                    Reset_toTransport[indexType].WaitOne();
                }

            }
            arrColorIsEnd[indexType] = true;
            Reset_toDestination[indexType].Set();
        }

        private void function_toTransport_TextBox(int indexType)
        {
            int i = 0;
            while (true)
            {
                //int maxShade = 257;
                int minIndex = -1;
                for (i = 0; i < N_columns_From * N_rows_From; i++)
                {
                    if (UC_From.arrControls[i] == null)
                        continue;
                    Control temp = UC_From.arrControls[i];
                    if (isValidType(temp, indexType))
                    {
                        minIndex = i;
                        //maxShade = temp.BackColor.R;
                    }
                }

                if (minIndex == -1)
                {
                    arrIndex_Color[indexType]++;
                    if (arrIndex_Color[indexType] == 1 || arrIndex_Color[indexType] == 2) //fill the transport with the other colors
                        continue;
                    else
                    {
                        arrColorIsEnd[indexType] = true;
                        Reset_toDestination[indexType].Set();
                        break;
                    }
                }

                if (arrCounter_Transport[indexType] < N_Transport)
                {
                    Invoke(new myDelegate(toTransport), minIndex, indexType);
                    Thread.Sleep(100);
                    arrCounter_Transport[indexType]++;
                }
                else
                {
                    Reset_toDestination[indexType].Set();
                    Reset_toTransport[indexType].WaitOne();
                }

            }
            arrColorIsEnd[indexType] = true;
            Reset_toDestination[indexType].Set();
        }

        private bool isValidType(Control c, int indexType)
        {
            switch (indexType)
            {
                case 0: //Button
                    return c.GetType().Name == "Button" &&
                           (c.BackColor.R != 0 && arrIndex_Color[0] == 0 || c.BackColor.G != 0 && arrIndex_Color[0] == 1 || c.BackColor.B != 0 && arrIndex_Color[0] == 2);

                case 1: //Label
                    return c.GetType().Name == "Label" &&
                            (c.BackColor.R != 0 && arrIndex_Color[1] == 0 || c.BackColor.G != 0 && arrIndex_Color[1] == 1 || c.BackColor.B != 0 && arrIndex_Color[1] == 2);

                case 2: //TextBox
                    return c.GetType().Name == "TextBox" &&
                             (c.BackColor.R != 0 && arrIndex_Color[2] == 0 || c.BackColor.G != 0 && arrIndex_Color[2] == 1 || c.BackColor.B != 0 && arrIndex_Color[2] == 2);
                default:
                    return false;
            }

        }

        private void toTransport(int indexSource, int indexType)
        {
            arrUC_Transport[indexType].arrControls[arrCounter_Transport[indexType]] = UC_From.arrControls[indexSource];
            arrUC_Transport[indexType].arrControls[arrCounter_Transport[indexType]].Location = new Point(3+ 31* arrCounter_Transport[indexType], 3);
            arrUC_Transport[indexType].Controls.Add(UC_From.arrControls[indexSource]);

            position_Transport[indexType] += UC_From.arrControls[indexSource].Width + 2;

            UC_From.Controls.Remove(UC_From.arrControls[indexSource]);
            UC_From.arrControls[indexSource] = null;
        }

        #endregion



        #region destination
        private void function_toDestination(object obj)
        {
            int indexType = (int)obj;
            while (true)
            {
                Reset_toDestination[indexType].WaitOne();

                for (int i = 0; i < arrCounter_Transport[indexType]; i++)
                {
                    Control temp = arrUC_Transport[indexType].arrControls[i];
                    if (temp != null)
                    {
                        Invoke(new myDelegate(toDestination), i, indexType);
                        Thread.Sleep(50);
                    }
                }
                arrCounter_Transport[indexType] = 0;
                position_Transport[indexType] = 0;
                if (arrColorIsEnd[indexType] == false)
                    Reset_toTransport[indexType].Set();
                else
                    break;
            }
        }

        private void toDestination(int indexTransport, int indexType)
        {
            arrUC_To[indexType].arrControls[arrCounter_Destination[indexType]]= arrUC_Transport[indexType].arrControls[indexTransport];
            arrUC_To[indexType].arrControls[arrCounter_Destination[indexType]].Location = new Point(position_Destination[indexType], 3);
            arrUC_To[indexType].Controls.Add(arrUC_Transport[indexType].arrControls[indexTransport]);

           
            position_Destination[indexType] += arrUC_Transport[indexType].arrControls[indexTransport].Width+2;
            arrCounter_Destination[indexType]++;

            arrUC_Transport[indexType].Controls.Remove(arrUC_Transport[indexType].arrControls[indexTransport]);
            arrUC_Transport[indexType].arrControls[indexTransport] = null;
        }
        #endregion

    }
}
