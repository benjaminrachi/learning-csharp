﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Event_RGBi_Square_Rectangle_Manager
{
    public partial class Form1 : Form
    {
        public event MyDelegate EventFromForm_onClick;//added
        public UserControl1 UCSelected;//added

        public UserControl1[] arrUC = new UserControl1[2];
        public Control ButtonLabel_MinMax_RectangleSquare_control = null;

        public Form1(string ButtonLabel, string MinMax, string RectangleSquare)
        {
            InitializeComponent();
            for (int i = 0; i < 2; i++)
            {
                arrUC[i] = new UserControl1();
                arrUC[i].Location = new Point(100, 27 + 85 * i);
                arrUC[i].EventFromUC_onClick  += new MyDelegate(Handler_EventFromUC_onClick); //added
                this.Controls.Add(arrUC[i]);
            }
            this.Text = ButtonLabel;
            Min_Max_label.Text = MinMax;
            Rectangle_Square_label.Text = RectangleSquare;

            if (ButtonLabel == "Button")
                ButtonLabel_MinMax_RectangleSquare_control = new Button();
            else
                ButtonLabel_MinMax_RectangleSquare_control = new Label();
            ButtonLabel_MinMax_RectangleSquare_control.Size = new Size(20, 20);
            ButtonLabel_MinMax_RectangleSquare_control.BackColor = Color.White;
            ButtonLabel_MinMax_RectangleSquare_control.Location = new Point(2, 60);
            this.Controls.Add(ButtonLabel_MinMax_RectangleSquare_control);
        }

        private void Handler_EventFromUC_onClick(myEventArgs eventargs)
        {
            eventargs.formClicked = this;
            UCSelected = eventargs.ucClicked;
            EventFromForm_onClick?.Invoke(eventargs);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
     }
}