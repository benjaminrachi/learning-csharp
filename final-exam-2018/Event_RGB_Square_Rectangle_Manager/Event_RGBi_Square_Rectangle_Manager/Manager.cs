﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Event_RGBi_Square_Rectangle_Manager
{
    public partial class Manager : Form
    {
        //added for the exercise
        public static int counterClick = 0;

        public UserControl1 UCSender;
        public Form1 FormSender;
        public UserControl1 UCReceiver;
        public Form1 FormReceiver;

        public static Form1 ButtonForm, LabelForm;
        public static String ButtonColor, LabelColor;
        public string ButtonShape, LabelShape;// Rectangle or Square

        public static List<Control> SortedListButton = new List<Control>(), SortedListLabel = new List<Control>();
        private Control ResultButton;
        private Control ResultLabel;

        private Form1 myForm_1 = null, myForm_2 = null;
        private Random myRand = new Random();
        public Manager()
        {
            InitializeComponent();

            string ButtonLabel1 = "Button", ButtonLabel2 = "Label";
            if (myRand.Next(2) == 0)
            {
                ButtonLabel1 = "Label"; ButtonLabel2 = "Button";
            }

            string MinMax1 = "Min", MinMax2 = "Max";
            if (myRand.Next(2) == 0)
            {
                MinMax1 = "Max"; MinMax2 = "Min";
            }

            string RectangleSquare1 = "Rectangle", RectangleSquare2 = "Square";
            if (myRand.Next(2) == 0)
            {
                RectangleSquare1 = "Square"; RectangleSquare2 = "Rectangle";
            }

            myForm_1 = new Form1(ButtonLabel1, MinMax1, RectangleSquare1);
            myForm_1.EventFromForm_onClick += new MyDelegate(Handler_EventFromForm_onClick); //added
            myForm_1.Show();
            myForm_2 = new Form1(ButtonLabel2, MinMax2, RectangleSquare2);
            myForm_2.EventFromForm_onClick += new MyDelegate(Handler_EventFromForm_onClick); //added
            myForm_2.Show();
        }

        private void Handler_EventFromForm_onClick(myEventArgs eventargs)
        {
            counterClick++;

            if (counterClick == 1)
            {
                FormSender = eventargs.formClicked;
                UCSender = eventargs.ucClicked;

            }
            else if (counterClick == 2)
            {
                //ignore if clicked twice on the same form
                if (FormSender == eventargs.formClicked)
                {
                    counterClick = 1;
                    return;
                }

                FormReceiver = eventargs.formClicked;
                UCReceiver = eventargs.ucClicked;

                ButtonForm = FormSender.Text == "Button" ? FormSender : FormReceiver;
                LabelForm = FormSender.Text == "Label" ? FormSender : FormReceiver;

                ButtonShape = ButtonForm.Rectangle_Square_label.Text == "Rectangle" ? "Rectangle" : "Square";
                LabelShape = LabelForm.Rectangle_Square_label.Text == "Rectangle" ? "Rectangle" : "Square";

                if (ButtonForm.radioButtonRed.Checked == true) ButtonColor = "Red";
                else if (ButtonForm.radioButtonBlue.Checked == true) ButtonColor = "Blue";
                else if (ButtonForm.radioButtonGreen.Checked == true) ButtonColor = "Green";

                if (LabelForm.radioButtonRed.Checked == true) LabelColor = "Red";
                else if (LabelForm.radioButtonBlue.Checked == true) LabelColor = "Blue";
                else if (LabelForm.radioButtonGreen.Checked == true) LabelColor = "Green";

                SortedListButton = getRequiredSortedList(ButtonForm.UCSelected, LabelForm.UCSelected, ButtonShape, ButtonColor, "Button");
                SortedListLabel = getRequiredSortedList(ButtonForm.UCSelected, LabelForm.UCSelected, LabelShape, LabelColor, "Label");

                ResultButton = ButtonForm.Min_Max_label.Text == "Max" ? SortedListButton.LastOrDefault() : SortedListButton.FirstOrDefault();
                ResultLabel = LabelForm.Min_Max_label.Text == "Max" ? SortedListLabel.LastOrDefault() : SortedListLabel.FirstOrDefault();
               
                //seif 1 updating the ButtonLabel_MinMax_RectangleSquare_control 
                if (ResultButton != null) ChangeApparence(ButtonForm.ButtonLabel_MinMax_RectangleSquare_control, ResultButton);
                if (ResultLabel != null) ChangeApparence(LabelForm.ButtonLabel_MinMax_RectangleSquare_control, ResultLabel);
            }

            else if (counterClick == 4)
            {
                //seif 2 updating the array of controls in the UC
                UpdatingArrControls(SortedListButton, ButtonForm.UCSelected);
                UpdatingArrControls(SortedListLabel, LabelForm.UCSelected);
                counterClick = 0;

            }
        }



        private List<Control> getRequiredSortedList(UserControl1 uCSelected1, UserControl1 uCSelected2, string shape, string color, string type)
        {
            List<Control> result = new List<Control>();
            var input = uCSelected1.arrControls.Concat(uCSelected2.arrControls);

            switch (shape)
            {
                case "Rectangle":
                    result = input
                        .Where(control =>
                            control.GetType().Name.Equals(type) &&
                            control.BackColor.Name.Equals(color)&&
                            control.Width != control.Height
                        )
                        .OrderBy(c1 => c1.Width * c1.Height)
                        .ToList();
                    break;
                case "Square":
                    result = input
                        .Where(control =>
                            control.GetType().Name.Equals(type) &&
                            control.BackColor.Name.Equals(color) &&
                            control.Width == control.Height
                        )
                        .OrderBy(c1 => c1.Width * c1.Height)
                        .ToList();
                    break;
            }
            return result;
        }

        private void ChangeApparence(Control buttonLabel_MinMax_RectangleSquare_control, Control result)
        {
            buttonLabel_MinMax_RectangleSquare_control.Size = result.Size;
            buttonLabel_MinMax_RectangleSquare_control.BackColor = result.BackColor;
        }

        private void UpdatingArrControls(List<Control> sortedList, UserControl1 uCSelected)
        {
            try
            {
                uCSelected.Controls.Clear();
                int currPosition = 2;
                for (int i = 0; i < sortedList.Count; i++)
                {
                    Control tempControl = sortedList[i];
                    tempControl.Location = new Point(currPosition, 2);
                    uCSelected.Controls.Add(tempControl);
                    currPosition += tempControl.Width + 2;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return;
            }
        }
    }
}