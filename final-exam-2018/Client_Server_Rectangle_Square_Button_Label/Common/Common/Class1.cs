using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Messaging;
using System.Drawing;
using System.Collections;

namespace Common
{
    public interface ICommon
    {
         void add_Client(ButtonLabel_Color_Size[] array);

        arrButtonLabel_Color_Size___Counter getData(string ButtonLabel, string RectangleSquare,  string ClientColor, int prevCounter);
    }

    [Serializable]
    public class ButtonLabel_Color_Size
    {
        public string ButtonLabel;
        public Size Size;
        public string Color;

        public ButtonLabel_Color_Size(string className, Size size, string backColor)
        {
            this.ButtonLabel = className;
            this.Size = size;
            this.Color = backColor;
        }
        public ButtonLabel_Color_Size() { }
    }

    
    [Serializable]
    public class arrButtonLabel_Color_Size___Counter
    {
        public int prevCounter;
        public ButtonLabel_Color_Size[] arrButLabColSize;

       

        public arrButtonLabel_Color_Size___Counter(int prevCounter, ButtonLabel_Color_Size[] arrButLabColSize)
        {
            this.prevCounter = prevCounter;
            this.arrButLabColSize = arrButLabColSize;
           
        }
        public arrButtonLabel_Color_Size___Counter() { }
    }
}
