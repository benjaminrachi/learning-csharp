using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels;
using System.Collections;
using Common;
using System.Linq;

namespace Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            HttpChannel chnl = new HttpChannel(1234);
            ChannelServices.RegisterChannel(chnl, false);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(ServerPart),
                "_Server_",
                WellKnownObjectMode.Singleton);

            Label label = new Label();
            label.Text = "Server is running...";
            label.BackColor = Color.BurlyWood;
            label.ForeColor = Color.Black;
            label.Name = "Arial";
            Controls.Add(label);
        }
    }

    class ServerPart : MarshalByRefObject, ICommon
    {

        private List<ButtonLabel_Color_Size> _allClientsControls = new List<ButtonLabel_Color_Size>();

        public void add_Client(ButtonLabel_Color_Size[] array)
        {
            foreach(var control in array)
            {
                _allClientsControls.Add(control);
            }
        }

        public arrButtonLabel_Color_Size___Counter getData(string ButtonLabel, string RectangleSquare, string ClientColor, int prevCounter)
        {
            
            var querryResult = _allClientsControls
                         .Where(control =>
                           control.ButtonLabel == ButtonLabel &&
                           control.Color == ClientColor
                         )
                         .OrderBy(control => control.Size.Width * control.Size.Height)
                         .ToList();
            switch (RectangleSquare)
            {
                case "Rectangle":
                    querryResult = querryResult.Where(control => control.Size.Width != control.Size.Height)
                                    .OrderBy(control => control.Size.Width * control.Size.Height)
                                    .ToList();
                    break;

                case "Square":
                    querryResult = querryResult.Where(control => control.Size.Width == control.Size.Height)
                                    .OrderBy(control => control.Size.Width * control.Size.Height)
                                    .ToList();
                    break;
            }


            arrButtonLabel_Color_Size___Counter response = new arrButtonLabel_Color_Size___Counter
            {
                prevCounter = querryResult.Count,
                arrButLabColSize = querryResult.ToArray()
            };

            return querryResult.Count == prevCounter ? null : response;
        }
    }
}