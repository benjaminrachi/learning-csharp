﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DragDrop_Event_ManagerForms_Square_Rectangle
{
    public class MyEventArgs : EventArgs
    {
        public UserControl1 UCsender;
        public UserControl1 UCreceiver;
        public Form1 FormSender;
        public Form1 FormReceiver;
    }
}
