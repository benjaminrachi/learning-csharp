﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace DragDrop_Event_ManagerForms_Square_Rectangle
{
    public partial class Form1 : Form
    {

        public UserControl1[] arrUC = new UserControl1[2];
        public Control Max_RectangleSquare_control = null;
        static Random myRand = new Random();
        public Control resultControl = null;
        //added for the exercise
        public event MyDelegate eventDragDropfinishFromForm;

        public Form1()
        {
            InitializeComponent();
            for (int i = 0; i < 2; i++)
            {
                arrUC[i] = new UserControl1(myRand);
                arrUC[i].Location = new Point(100, 27 + 85 * i);
                arrUC[i].eventOnMouseDownFromUC += new MyDelegate(eventOnMouseDownFromUcHandler); //added
                arrUC[i].eventDragDropfinishFromUC += new MyDelegate(eventDragDropfinishFromUCHandler); //added

                this.Controls.Add(arrUC[i]);
            }
        }

        public void eventOnMouseDownFromUcHandler(MyEventArgs eventargs)
        {
            eventargs.FormSender = this;
            DoDragDrop(eventargs, DragDropEffects.All);

        }

        private void eventDragDropfinishFromUCHandler(MyEventArgs eventargs)
        {
            eventargs.FormReceiver = this;
            if (eventDragDropfinishFromForm != null)
            {
                eventDragDropfinishFromForm(eventargs);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (resultControl != null)
                this.Controls.Remove(resultControl);
            if (((Control) sender).Text == "Button")
                resultControl = new Button();
            else
                resultControl = new Label();

            resultControl.BackColor = Color.White;
            resultControl.Location = new Point(2, 62);
            resultControl.Size = new Size(30, 30);
            this.Controls.Add(resultControl);
        }

    }
}
