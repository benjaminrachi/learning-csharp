﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DragDrop_Event_ManagerForms_Square_Rectangle
{
    //Benjamin
    public partial class ManagerForms : Form
    {
        private Form1[] arrForms = new Form1[2];
        private Random rand_ManagerForms = new Random();

        public ManagerForms()
        {
            InitializeComponent();

            for (int i = 0; i < 2; i++)
            {
                arrForms[i] = new Form1();
                arrForms[i].Show();
                arrForms[i].eventDragDropfinishFromForm += new MyDelegate(HandlerEventDragDropfinishFromForm);
            }

            if (rand_ManagerForms.Next(2) == 0)
            {
                arrForms[0].Text = "Rectangle";                
                arrForms[1].Text = "Square";
            }
            else
            {
                arrForms[0].Text = "Square";                
                arrForms[1].Text = "Rectangle";
            }

            switch (rand_ManagerForms.Next(6))
            {
                case 0: arrForms[0].label_RedGreenBlue.Text = "Red";
                    arrForms[1].label_RedGreenBlue.Text = "Green"; break;
                case 1: arrForms[0].label_RedGreenBlue.Text = "Red";
                    arrForms[1].label_RedGreenBlue.Text = "Blue"; break;
                case 2: arrForms[0].label_RedGreenBlue.Text = "Green";
                    arrForms[1].label_RedGreenBlue.Text = "Red"; break;
                case 3: arrForms[0].label_RedGreenBlue.Text = "Green";
                    arrForms[1].label_RedGreenBlue.Text = "Blue"; break;
                case 4: arrForms[0].label_RedGreenBlue.Text = "Blue";
                    arrForms[1].label_RedGreenBlue.Text = "Red"; break;
                case 5: arrForms[0].label_RedGreenBlue.Text = "Blue";
                    arrForms[1].label_RedGreenBlue.Text = "Green"; break;
            }

            if (rand_ManagerForms.Next(2) == 0)
            {
                arrForms[0].label_MinMax.Text = "Min";
                arrForms[1].label_MinMax.Text = "Max";
            }
            else
            {
                arrForms[0].label_MinMax.Text = "Max";
                arrForms[1].label_MinMax.Text = "Min";
            }

            this.FormBorderStyle = FormBorderStyle.None;
            this.TransparencyKey = SystemColors.Control;
            this.ShowInTaskbar = false;
        }

        public void HandlerEventDragDropfinishFromForm(MyEventArgs eventargs)
        {
            List<Control> sortedListSender = GetRequiredSortedList(eventargs.FormSender.Text, eventargs.FormSender.label_MinMax, eventargs.FormSender.label_RedGreenBlue, eventargs.UCsender, eventargs.UCreceiver, eventargs.FormSender.resultControl);
            List<Control> sortedListReceiver = GetRequiredSortedList(eventargs.FormReceiver.Text, eventargs.FormReceiver.label_MinMax, eventargs.FormReceiver.label_RedGreenBlue, eventargs.UCsender, eventargs.UCreceiver, eventargs.FormReceiver.resultControl);
            
            Control resultSender = GetPreferedControl(sortedListSender, eventargs.FormSender.label_MinMax);
            Control resultReceiver = GetPreferedControl(sortedListReceiver, eventargs.FormReceiver.label_MinMax);
            
            //seif 1 changing the result control apparence
            ChangeApparence(eventargs.FormSender.resultControl, resultSender);
            ChangeApparence(eventargs.FormReceiver.resultControl, resultReceiver);

            //seif 2 updating the array of controles in the UC
            UpdatingArrControls(sortedListSender, eventargs.UCsender);
            UpdatingArrControls(sortedListReceiver, eventargs.UCreceiver);

        }

        public List<Control> GetRequiredSortedList(string textRectangleSquare, Label labelMinMax, Label labelRedGreenBlue, UserControl1 ucSender, UserControl1 ucReceiver, Control resultControl)
        {
            List<Control> result= new List<Control>();
            var input =(ucSender.arrControls.Concat(ucReceiver.arrControls));

            switch (textRectangleSquare)
            {
                case "Rectangle":
                    result = input
                        .Where(control =>
                            control.GetType() == resultControl.GetType() &&
                            control.BackColor.Equals(Color.FromName(labelRedGreenBlue.Text)) &&
                            control.Width != control.Height
                        )
                        .OrderBy(c1 => c1.Width * c1.Height
                        )
                        .ToList();
                    break;
                case "Square":
                    result = input
                        .Where(control =>
                            control.GetType() == resultControl.GetType() &&
                            control.BackColor.Equals(Color.FromName(labelRedGreenBlue.Text)) &&
                            control.Width == control.Height
                        )
                        .OrderBy(c1 => c1.Width * c1.Height
                        )
                        .ToList();
                    break;
            }
            return result;
        }

        public Control GetPreferedControl(List<Control> sortedList, Label label_MinMax)
        {
            switch (label_MinMax.Text)
            {
                case "Min":
                    return sortedList.First();
                case "Max":
                    return sortedList.Last();
            }
            return sortedList.First();
        }

        public static void ChangeApparence(Control resultControl, Control result)
        {
            resultControl.Size = result.Size;
            resultControl.BackColor = result.BackColor;

        }

        public void UpdatingArrControls(List<Control> sortedList, UserControl1 uc)
        {
            uc.Controls.Clear();
            int currPosition = 2;
            for (int i = 0; i < sortedList.Count; i++)
            {
                Control tempControl = sortedList[i];
                tempControl.Location = new Point(currPosition, 2);
                uc.Controls.Add(tempControl);
                currPosition += tempControl.Width + 2;
            }
        }
    }
}
