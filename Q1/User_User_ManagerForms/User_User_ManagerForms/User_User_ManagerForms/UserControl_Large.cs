﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace User_User_ManagerForms
{
    public partial class UserControl_Large : UserControl
    {
        public UserControl_Small mUser_Small;
        static Random myRand = new Random();

        public event MyDelegate EventFromUClarge_onClick;//added
        public UserControl_Small UCSmallSelected;//added

        public UserControl_Large(int index)
        {
            InitializeComponent();

            mUser_Small = new UserControl_Small(myRand) {Location = new Point(240, 3)};
            mUser_Small.EventFromUCsmall_onClick += new MyDelegate(Handler_EventFromUCsmall_onClick); //added
            this.Width = mUser_Small.arrControls.Length * 160 + 80;

            this.Controls.Add(mUser_Small);
            label1.Text = index == 0 ? "Rectangle" : "Square";
        }

        private void Handler_EventFromUCsmall_onClick(MyEventArgs eventargs)
        {
            eventargs.UCLargeSender = this;
            UCSmallSelected = eventargs.UCSmallSender;
            EventFromUClarge_onClick?.Invoke(eventargs);
        }
    }
}
