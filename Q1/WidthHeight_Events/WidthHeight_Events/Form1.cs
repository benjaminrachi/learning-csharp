﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Collections;

namespace WidthHeight_Events
{
    //Benjamin
    public partial class Form1 : Form
    {
        private UserControl1[] arrUserControl;
        private int arrUser_size = 2;

        //added for the exercise
        public static int counterClick=0;
        public static UserControl1 ucButton, ucLabel;
        public static Form1 firstForm, secondForm, ButtonForm, LabelForm;
        public static String ButtonColor, LabelColor;
        public static Button maxButton =new Button();
        public static Label minLabel = new Label();
        public static List<Control> SortedListButton= new List<Control>(), SortedListLabel= new List<Control>();
        
        public Form1(int counter)
        {
            InitializeComponent();
            if (counter == 1)
            {
                this.Text = "Button";
                labelMinMaxSize.Text = "MAX SIZE";
            }
            else
            {
                this.Text = "Label";
                labelMinMaxSize.Text = "MIN SIZE";
            }
            
            arrUserControl = new UserControl1[arrUser_size];
            for (int i = 0; i < arrUser_size; i++)
            {
                arrUserControl[i] = new UserControl1();

                //added for the exercise
                arrUserControl[i].ClickUcEvent+= new Mydelegate(HandlerUcClick);
               
                arrUserControl[i].Location = new Point(58, 35 + 127 * i);
                this.Controls.Add(arrUserControl[i]);
            }

            if (counter > 1)
            {
                Form1 temp = new Form1(counter - 1);
                temp.Show();
            }
        }

        public void HandlerUcClick(MyEventArgs e)
        {
            if (this.Text == "Button")
            {
                ucButton=e.ucClicked;
                ButtonForm = this;
                if (checkBoxBlue.Checked) ButtonColor = "Blue";
                else if (checkBoxRed.Checked) ButtonColor = "Red";
                else if (checkBoxGreen.Checked) ButtonColor = "Green";

            }
            else
            {
                ucLabel = e.ucClicked;
                LabelForm = this;
                if (checkBoxBlue.Checked) LabelColor = "Blue";
                else if (checkBoxRed.Checked) LabelColor = "Red";
                else if (checkBoxGreen.Checked) LabelColor = "Green";
            }
            counterClick++;

            if (counterClick == 1)
            {
                firstForm = this;
            }
            if (counterClick == 2)
            {
                Console.WriteLine("starting seif 1");
                secondForm = this;
                if (ucButton != null && ucLabel != null)
                {
                    SortedListButton = getRequiredSortedList(ButtonColor, ucButton, ucLabel,"Button");
                    SortedListLabel = getRequiredSortedList(LabelColor, ucButton, ucLabel, "Label");

                    maxButton = (Button) SortedListButton.Last();
                    minLabel = (Label) SortedListLabel.First();
                }
                //seif 1 updating the min max labels
                updatingWHLabels(ButtonForm, maxButton);
                updatingWHLabels(LabelForm, minLabel);
                //ButtonForm.labelWidth.Text= maxButton.Width.ToString();
                //ButtonForm.labelHeight.Text = maxButton.Height.ToString();
                //LabelForm.labelWidth.Text = minLabel.Width.ToString();
                //LabelForm.labelHeight.Text = minLabel.Height.ToString();
            }
            else if (counterClick == 4)
            {
                //seif 2 updating the array of controls in the UC
                updatingArrControls(SortedListButton, ucButton);
                updatingArrControls(SortedListLabel, ucLabel);
                counterClick = 0;

            }
        }

        public void updatingWHLabels(Form1 Form, Control ctrl)
        {
            Form.labelWidth.Text=ctrl.Width.ToString();
            Console.WriteLine("width ="+Form.labelWidth.Text);
            Form.labelHeight.Text = ctrl.Height.ToString();
            Console.WriteLine("height =" + Form.labelHeight.Text);
        }

        public List<Control> getRequiredSortedList( String FormColor, UserControl1 FirstUC, UserControl1 SecondUC, String Button_Label)
        {
           
            List<Control> result = new List<Control>();
            var input = FirstUC.arrControls.Concat(SecondUC.arrControls);
            result = input
                .Where(control =>
                    control.GetType().Name.Equals(Button_Label) &&
                    control.BackColor.Equals(Color.FromName(FormColor))
                )
                .OrderBy(c1 => c1.Width * c1.Height
                )
                .ToList();
            return result;
        }

        public void updatingArrControls(List<Control> sortedList, UserControl1 UC)
        {
            UC.Controls.Clear();
            int currPosition = 2;
            for (int i = 0; i < sortedList.Count; i++)
            {
                Control tempControl = sortedList[i];
                tempControl.Location = new Point(currPosition, 2);
                UC.Controls.Add(tempControl);
                currPosition += tempControl.Width + 2;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
