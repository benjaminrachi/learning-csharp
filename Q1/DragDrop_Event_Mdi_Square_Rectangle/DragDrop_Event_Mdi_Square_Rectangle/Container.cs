﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DragDrop_Event_Mdi_Square_Rectangle
{
    public partial class Container : Form
    {
        private Child[] arrChild = new Child[2];

        public Container()
        {
            InitializeComponent();
            for (int i = 0; i < 2; i++)
            {
                arrChild[i] = new Child();
                arrChild[i].StartPosition = FormStartPosition.Manual;
                arrChild[i].Location = new Point(0, 238 * i);
                arrChild[i].Show();
                arrChild[i].MdiParent = this;
                arrChild[i].eventDragDropfinishFromForm += new MyDelegate(HandlerEventDragDropfinishFromForm);

                if (i == 0)
                {
                    arrChild[i].Text = "Button";
                    arrChild[i].Max_RectangleSquare_control = new Button();
                }
                else
                {
                    arrChild[i].Text = "Label";
                    arrChild[i].Max_RectangleSquare_control = new Label();
                }
                arrChild[i].Max_RectangleSquare_control.BackColor = Color.White;
                arrChild[i].Max_RectangleSquare_control.Location = new Point(2, 60);
                arrChild[i].Max_RectangleSquare_control.Size = new Size(30, 30);
                arrChild[i].Controls.Add(arrChild[i].Max_RectangleSquare_control);
            }
        }

        public void HandlerEventDragDropfinishFromForm(MyEventArgs eventargs)
        {
            List<Control> sortedListSender = getRequiredSortedList(eventargs.FormSender.Max_RectangleSquare.Text, eventargs.FormSender.CheckBoxColor, eventargs.UCsender, eventargs.UCreceiver, eventargs.FormSender.Max_RectangleSquare_control);
            List<Control> sortedListReceiver = getRequiredSortedList(eventargs.FormReceiver.Max_RectangleSquare.Text, eventargs.FormReceiver.CheckBoxColor, eventargs.UCsender, eventargs.UCreceiver, eventargs.FormReceiver.Max_RectangleSquare_control);

            //List<Control> sortedListSender = getRequiredSortedList(eventargs.FormSender.Max_RectangleSquare.Text, eventargs.FormSender.CheckBoxColor, eventargs.UCsender, eventargs.UCreceiver, eventargs.FormSender.Text);
            //List<Control> sortedListReceiver = getRequiredSortedList(eventargs.FormReceiver.Max_RectangleSquare.Text, eventargs.FormReceiver.CheckBoxColor, eventargs.UCsender, eventargs.UCreceiver, eventargs.FormReceiver.Text);

            if (sortedListSender == null || sortedListReceiver == null || sortedListSender.Count == 0 || sortedListReceiver.Count == 0) return;

            Control resultSender = sortedListSender.Last(); //take always the maximum
            Control resultReceiver = sortedListReceiver.Last(); //take always the maximum

            //seif 1 changing the result control apparence
            changeApparence(eventargs.FormSender.Max_RectangleSquare_control, resultSender);
            changeApparence(eventargs.FormReceiver.Max_RectangleSquare_control, resultReceiver);

            //seif 2 updating the array of controls in the UC
            updatingArrControls(sortedListSender, eventargs.UCsender);
            updatingArrControls(sortedListReceiver, eventargs.UCreceiver);

        }

        //public List<Control> getRequiredSortedList(String textRectangleSquare,  String StringRedGreenBlue, UserControl1 uCsender, UserControl1 uCreceiver, String textButtonLabel)
        public List<Control> getRequiredSortedList(String textRectangleSquare,  String StringRedGreenBlue, UserControl1 uCsender, UserControl1 uCreceiver, Control resultControl)
        {
            List<Control> result = new List<Control>();
            var input = (uCsender.arrControls.Concat(uCreceiver.arrControls));

            if (textRectangleSquare.Equals("Max Rectangle")) 
            {
                result = input
                  .Where(control =>
                      control.GetType().Equals(resultControl.GetType()) &&
                      control.BackColor.Equals(Color.FromName(StringRedGreenBlue)) &&
                      control.Width != control.Height
                  )
                  .OrderBy(c1 => c1.Width * c1.Height
                  )
                  .ToList();
            }
            else if (textRectangleSquare.Equals("Max Square"))
            {
                result = input
                    .Where(control =>
                        control.GetType().Equals(resultControl.GetType()) &&
                        control.BackColor.Equals(Color.FromName(StringRedGreenBlue)) &&
                        control.Width == control.Height
                    )
                    .OrderBy(c1 => c1.Width * c1.Height
                    )
                    .ToList();
            }
            return result;
        }

        public static void changeApparence(Control resultControl, Control result)
        {
            resultControl.Size = result.Size;
            resultControl.BackColor = result.BackColor;

        }

        public void updatingArrControls(List<Control> sortedList, UserControl1 uc)
        {
            uc.Controls.Clear();
            int currPosition = 2;
            for (int i = 0; i < sortedList.Count; i++)
            {
                Control tempControl = sortedList[i];
                tempControl.Location = new Point(currPosition, 2);
                uc.Controls.Add(tempControl);
                currPosition += tempControl.Width + 2;
            }
        }
    }
}
