﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DragDrop_Event_Mdi_Square_Rectangle
{
    public class MyEventArgs: EventArgs
    {
        public UserControl1 UCsender = null;
        public UserControl1 UCreceiver = null;
        public Child FormSender = null;
        public Child FormReceiver = null;
    }
}
