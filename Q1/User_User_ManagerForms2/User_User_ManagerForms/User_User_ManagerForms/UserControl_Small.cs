﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace User_User_ManagerForms
{
    public delegate void MyDelegate(MyEventArgs eventArgs);//added
    public partial class UserControl_Small : UserControl
    {
        public Control[] arrControls;
        private Random tempRand;

        public event MyDelegate EventFromUCsmall_onClick;//added

        public UserControl_Small(Random tRand)
        {
            InitializeComponent();
            tempRand = tRand;
            int arrSize = tempRand.Next(7, 11);
            arrControls = new Control[arrSize];
            this.Click += UserControl_Small_Click;
            int currPosition = 2;

            for (int i = 0; i < arrSize; i++)
            {
                if (tempRand.Next(2) == 0)
                    arrControls[i] = new Label();
                else
                    arrControls[i] = new Button();

                int img = tempRand.Next(1, 4);
                
                arrControls[i].Location = new Point(currPosition, 3);
                int temp = tempRand.Next(40, 70);
                switch (tempRand.Next(4))
                {
                    case 0: arrControls[i].Size = new Size(temp, temp); break;
                    case 1: arrControls[i].Size = new Size(2 * temp, 2 * temp); break;
                    case 2: arrControls[i].Size = new Size(2 * temp, temp); break;
                    case 3: arrControls[i].Size = new Size(temp, 2 * temp); break;
                }
                arrControls[i].BackgroundImage = new Bitmap(img + ".jpg");
                arrControls[i].BackgroundImageLayout = ImageLayout.Stretch;
                arrControls[i].Tag =  img + ".jpg";
                switch (tempRand.Next(3))
                {
                    case 0: arrControls[i].BackColor = Color.Red; break;
                    case 1: arrControls[i].BackColor = Color.Green; break;
                    case 2: arrControls[i].BackColor = Color.Blue; break;
                }

                currPosition += arrControls[i].Size.Width + 2;
                arrControls[i].ForeColor = Color.Blue;
                arrControls[i].Font = new System.Drawing.Font("Microsoft Sans Serif", 14, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
                arrControls[i].Text = arrControls[i].Size.Width + "\n" + arrControls[i].Size.Height;
                this.Controls.Add(arrControls[i]);
                Console.WriteLine(currPosition);
            }
            this.Width = 7 * 160;
        }


        private void UserControl_Small_Click(object sender, EventArgs e)
        {
            MyEventArgs args = new MyEventArgs { UCSmallSender = this };

            EventFromUCsmall_onClick?.Invoke(args);// =someone is handling this event
        }
    }
}
