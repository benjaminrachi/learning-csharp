﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User_User_ManagerForms
{
    public class MyEventArgs : EventArgs
    {
        public UserControl_Small UCSmallSender;
        public UserControl_Large UCLargeSender;
        public Form1 FormSender;
    }
}
