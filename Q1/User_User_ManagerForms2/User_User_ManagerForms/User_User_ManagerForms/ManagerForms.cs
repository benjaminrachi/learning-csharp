﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
namespace User_User_ManagerForms
{
    //BENJAMIN
    public partial class ManagerForms : Form
    {
        private int counterForms = 2;
        public Form1[] arrForm1;

        //added for the exercise
        public static int counterClick = 0;

        public static Form1 ButtonForm, LabelForm;
        public static String ButtonColor, LabelColor;
        public double ButtonArea, LabelArea;

        public UserControl_Small UCSmallSender;
        public UserControl_Large UCLargeSender;
        public Form1 FormSender;
        public UserControl_Small UCSmallReceiver;
        public UserControl_Large UCLargeReceiver;
        public Form1 FormReceiver;
        private Button maxButton;
        private Label maxLabel;
        public static List<Control> SortedListButton = new List<Control>(), SortedListLabel = new List<Control>(),
                                    SortedListButtonBelow = new List<Control>(), SortedListLabelBelow = new List<Control>();

        public ManagerForms()
        {
            InitializeComponent();
            arrForm1 = new Form1[counterForms];
            for (int i = 0; i < counterForms; i++)
            {
                arrForm1[i] = new Form1();
                arrForm1[i].Show();
                arrForm1[i].EventFromForm_onClick += new MyDelegate(Handler_EventFromForm_onClick); //added
            }

            arrForm1[0].Text = "Button";
            arrForm1[1].Text = "Label";

            this.FormBorderStyle = FormBorderStyle.None;
            this.TransparencyKey = SystemColors.Control;
            this.ShowInTaskbar = false;
        }

        private void Handler_EventFromForm_onClick(MyEventArgs eventargs)
        {
            counterClick++;

            if (counterClick == 1)
            {
                FormSender = eventargs.FormSender;
                UCLargeSender = eventargs.UCLargeSender;
                UCSmallSender = eventargs.UCSmallSender;

            }
            else if (counterClick == 2)
            {
                //ignore if clicked twice on the same form
                if (FormSender == eventargs.FormSender || UCLargeSender == eventargs.UCLargeSender || UCSmallSender == eventargs.UCSmallSender)
                {
                    counterClick = 1;
                    return;
                }

                FormReceiver = eventargs.FormSender;
                UCLargeReceiver = eventargs.UCLargeSender;
                UCSmallReceiver = eventargs.UCSmallSender;

                ButtonForm = FormSender.Text == "Button" ? FormSender : FormReceiver;
                LabelForm = FormSender.Text == "Label" ? FormSender : FormReceiver;

                ButtonColor = ButtonForm.radioButton3.Checked == true ? "Colored" : "Gray";
                LabelColor = LabelForm.radioButton3.Checked == true ? "Colored" : "Gray";

                ButtonArea = double.Parse(ButtonForm.width.Text) * double.Parse(ButtonForm.height.Text);
                LabelArea = double.Parse(LabelForm.width.Text) * double.Parse(LabelForm.height.Text);


                if (ButtonForm.UCLargeSelected != null && LabelForm.UCLargeSelected != null)
                {
                    SortedListButton = getRequiredSortedList(ButtonForm.UCLargeSelected.label1.Text, ButtonForm.UCLargeSelected, LabelForm.UCLargeSelected, "Button");
                    SortedListLabel = getRequiredSortedList(LabelForm.UCLargeSelected.label1.Text, ButtonForm.UCLargeSelected, LabelForm.UCLargeSelected, "Label");

                    SortedListButtonBelow = SortedListButton.Where(control => control.Width * control.Height < ButtonArea).OrderBy(c1 => c1.Width * c1.Height).ToList();
                    SortedListLabelBelow = SortedListLabel.Where(control => control.Width * control.Height < LabelArea).OrderBy(c1 => c1.Width * c1.Height).ToList();

                    if (SortedListButtonBelow.Count>0)
                    {
                        maxButton = ButtonColor == "Colored" ? (Button)SortedListButtonBelow?.LastOrDefault() : (Button)convertToGray(SortedListButtonBelow?.LastOrDefault(), true);
                    }


                    if (SortedListLabelBelow.Count>0)
                    {
                        maxLabel = LabelColor == "Colored" ? (Label)SortedListLabelBelow?.LastOrDefault() : (Label)convertToGray(SortedListLabelBelow?.LastOrDefault(), false);
                    }

                    //seif 1 updating the max labels
                    if (maxButton != null) ChangeApparence(ButtonForm.UCLargeSelected.labelCounter, maxButton);
                    if (maxLabel != null) ChangeApparence(LabelForm.UCLargeSelected.labelCounter, maxLabel);
                }

            }
            else if (counterClick == 4)
            {
                //seif 2 updating the array of controls in the UC
                UpdatingArrControls(SortedListButton, maxButton, ButtonColor, ButtonForm.UCLargeSelected.UCSmallSelected, ButtonArea);
                UpdatingArrControls(SortedListLabel, maxLabel, LabelColor, LabelForm.UCLargeSelected.UCSmallSelected, LabelArea);
                counterClick = 0;

            }
        }

        private List<Control> getRequiredSortedList(string textRectangleSquare, UserControl_Large ucLarge1, UserControl_Large ucLarge2, string button_label)
        {
            List<Control> result = new List<Control>();
            var input = ucLarge1.UCSmallSelected.arrControls.Concat(ucLarge2.UCSmallSelected.arrControls);

            switch (textRectangleSquare)
            {
                case "Rectangle":
                    result = input
                        .Where(control =>
                            control.GetType().Name.Equals(button_label) &&
                            control.Width != control.Height
                        )
                        .OrderBy(c1 => c1.Width * c1.Height)
                        .ToList();
                    break;
                case "Square":
                    result = input
                        .Where(control =>
                            control.GetType().Name.Equals(button_label) &&
                            control.Width == control.Height
                        )
                        .OrderBy(c1 => c1.Width * c1.Height)
                        .ToList();
                    break;
            }
            return result;
        }

        public static void ChangeApparence(Control resultControl, Control result)
        {
            resultControl.Size = result.Size;
            resultControl.BackColor = result.BackColor;
            resultControl.Tag = result.Tag;
            resultControl.BackgroundImage = result.BackgroundImage;
            resultControl.BackgroundImageLayout = result.BackgroundImageLayout;
            resultControl.ForeColor = result.ForeColor;
            resultControl.Font = result.Font;
            resultControl.Text = result.Text;
        }

        public void UpdatingArrControls(List<Control> sortedList, Control reference, string color, UserControl_Small ucSmallSelected, double area)
        {
            try
            {
                List<Control> result = new List<Control>();
                result = sortedList
                    .Where(control => 
                        control.Tag.ToString().Equals(reference.Tag.ToString()) &&
                        control.Width * control.Height >= area
                    )
                    .ToList();

                ucSmallSelected.Controls.Clear();
                int currPosition = 2;
                for (int i = 0; i < result.Count; i++)
                {
                    Control tempControl = color == "Colored" ? result[i] : convertToGray(result[i], result[i].GetType().Name == "Button");
                    tempControl.Location = new Point(currPosition, 2);
                    ucSmallSelected.Controls.Add(tempControl);
                    currPosition += tempControl.Width + 2;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return;
            }
        }

        public static Control convertToGray(Control ctrl, bool isButton)
        {
            Bitmap img = new Bitmap((Bitmap)ctrl.BackgroundImage);

            for (int i = 0; i < img.Width; i++)
            {
                for (int j = 0; j < img.Height; j++)
                {
                    Color c = img.GetPixel(i, j);
                    int avg = c.R;
                    img.SetPixel(i, j, Color.FromArgb(avg, avg, avg));
                }
            }

            if (isButton)
            {
                return new Button()
                {
                    Tag = ctrl.Tag,
                    Text = ctrl.Text,
                    Size = ctrl.Size,
                    BackgroundImage = img,
                    BackgroundImageLayout = ctrl.BackgroundImageLayout,
                    TextAlign = ContentAlignment.MiddleCenter
                };
            }

            else
            {
                return new Label()
                {
                    Tag = ctrl.Tag,
                    Text = ctrl.Text,
                    Size = ctrl.Size,
                    BackgroundImage = img,
                    BackgroundImageLayout = ctrl.BackgroundImageLayout,
                    TextAlign = ContentAlignment.MiddleCenter
                };
            }
        }

        void convertToGray(Control ctrl)
        {
            Bitmap img = (Bitmap)ctrl.BackgroundImage;
            for (int i = 0; i < img.Width; i++)
                for (int j = 0; j < img.Height; j++)
                {
                    Color c = img.GetPixel(i, j);
                    int avg = c.R;
                    img.SetPixel(i, j, Color.FromArgb(avg, avg, avg));
                }
            ctrl.Refresh();
        }
       
    }
}
