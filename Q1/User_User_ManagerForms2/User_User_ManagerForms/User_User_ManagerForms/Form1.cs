﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace User_User_ManagerForms
{
    public partial class Form1 : Form
    {
        UserControl_Large[] arrUser_Large;

        public event MyDelegate EventFromForm_onClick;//added
        public UserControl_Large UCLargeSelected;//added

        public Form1()
        {
            InitializeComponent();
            arrUser_Large = new UserControl_Large[2];
            for (int i = 0; i < 2; i++)
            {
                arrUser_Large[i] = new UserControl_Large(i);
                arrUser_Large[i].Location = new Point(80, 30 + arrUser_Large[i].Height * i);
                arrUser_Large[i].EventFromUClarge_onClick += new MyDelegate(Handler_EventFromUClarge_onClick); //added
                this.Controls.Add(arrUser_Large[i]);
            }
            this.Width = 7 * 160 + 80;
            this.Height = 300;
        }

        private void Handler_EventFromUClarge_onClick(MyEventArgs eventargs)
        {
            eventargs.FormSender = this;
            UCLargeSelected = eventargs.UCLargeSender;
            EventFromForm_onClick?.Invoke(eventargs);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

    }
}
