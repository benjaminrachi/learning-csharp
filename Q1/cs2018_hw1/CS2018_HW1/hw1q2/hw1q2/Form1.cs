﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hw1q2
{
    public partial class Form1 : Form
    {

        List<Point> allLocations = new List<Point>(16);
        Dictionary<string, Button> textToButton = new Dictionary<string, Button>();
        Dictionary<Point, List<Point>> pointToNeighborsLocation = new Dictionary<Point, List<Point>>();
        private Random rnd = new Random();
        bool isInMovement = false;
        Point origin;
        Point destination;
        Button buttonToBeMoved;

        public Form1()
        {
            InitializeComponent();

            foreach (Control c in this.Controls)
            {
                if( c is Button)
                {
                    Button b = (Button)c;
                    allLocations.Add(b.Location);
                    b.Tag = b.Location;
                    //pointToButton.Add(b.Location, b);
                    textToButton.Add(b.Text, b);
                }
                
            }


            foreach (Control c in this.Controls)
            {
               

                if (c is Button)
                {
                    List<Point> neighbors = new List<Point>();
                    Button b = (Button)c;

                    switch (b.Text.ToString())
                    {
                        case "1":
                            neighbors.Add(textToButton["2"].Location);
                            neighbors.Add(textToButton["5"].Location);
                            break;

                        case "2":
                            neighbors.Add(textToButton["1"].Location);
                            neighbors.Add(textToButton["3"].Location);
                            neighbors.Add(textToButton["6"].Location);
                            break;

                        case "3":
                            neighbors.Add(textToButton["2"].Location);
                            neighbors.Add(textToButton["4"].Location);
                            neighbors.Add(textToButton["7"].Location);
                            break;

                        case "4":
                            neighbors.Add(textToButton["3"].Location);
                            neighbors.Add(textToButton["8"].Location);
                            break;

                        case "5":
                            neighbors.Add(textToButton["1"].Location);
                            neighbors.Add(textToButton["6"].Location);
                            neighbors.Add(textToButton["9"].Location);
                            break;

                        case "6":
                            neighbors.Add(textToButton["2"].Location);
                            neighbors.Add(textToButton["5"].Location);
                            neighbors.Add(textToButton["7"].Location);
                            neighbors.Add(textToButton["10"].Location);
                            break;

                        case "7":
                            neighbors.Add(textToButton["3"].Location);
                            neighbors.Add(textToButton["6"].Location);
                            neighbors.Add(textToButton["8"].Location);
                            neighbors.Add(textToButton["11"].Location);
                            break;

                        case "8":
                            neighbors.Add(textToButton["4"].Location);
                            neighbors.Add(textToButton["7"].Location);
                            neighbors.Add(textToButton["12"].Location);
                            break;

                        case "9":
                            neighbors.Add(textToButton["5"].Location);
                            neighbors.Add(textToButton["10"].Location);
                            neighbors.Add(textToButton["13"].Location);
                            break;

                        case "10":
                            neighbors.Add(textToButton["6"].Location);
                            neighbors.Add(textToButton["9"].Location);
                            neighbors.Add(textToButton["11"].Location);
                            neighbors.Add(textToButton["14"].Location);
                            break;

                        case "11":
                            neighbors.Add(textToButton["7"].Location);
                            neighbors.Add(textToButton["10"].Location);
                            neighbors.Add(textToButton["12"].Location);
                            neighbors.Add(textToButton["15"].Location);
                            break;

                        case "12":
                            neighbors.Add(textToButton[""].Location);
                            neighbors.Add(textToButton["8"].Location);
                            neighbors.Add(textToButton["11"].Location);
                            break;


                        case "13":
                            neighbors.Add(textToButton["9"].Location);
                            neighbors.Add(textToButton["14"].Location);
                            break;

                        case "14":
                            neighbors.Add(textToButton["10"].Location);
                            neighbors.Add(textToButton["13"].Location);
                            neighbors.Add(textToButton["15"].Location);
                            break;

                        case "15":
                            neighbors.Add(textToButton[""].Location);
                            neighbors.Add(textToButton["11"].Location);
                            neighbors.Add(textToButton["14"].Location);
                            break;

                        case "":
                            neighbors.Add(textToButton["12"].Location);
                            neighbors.Add(textToButton["15"].Location);
                            break;
                    }

                    pointToNeighborsLocation.Add(b.Location, neighbors);


                }

            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            newGame();
        }

        public bool isSucces()
        {

            foreach (Control c in this.Controls)
            {
                if (c is Button)
                {

                    if (c.Text.ToString() != c.Tag.ToString())
                    {
                        return false;

                    }

                }

            }

            return true;

        }

        public bool isLost()
        {

            if ( (textToButton["15"].Location == textToButton["14"].Location) && (textToButton["14"].Location == textToButton["15"].Location) )
                return true;

            else
                return false;
        }

        public void afterTurn()
        {
            DialogResult dialogResult;

            if (isSucces())
            {
                dialogResult = MessageBox.Show("You win" + Environment.NewLine + "Do you want to start a new game?", "Confirmation", MessageBoxButtons.YesNoCancel);
                //MessageBox.Show("You win \t Do you want to start a new game?", "Form1",MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);

                if (dialogResult == DialogResult.Yes)
                {
                    newGame();
                }

                else if (dialogResult == DialogResult.No)
                {
                    Close();
                }
            }
            else if (isLost())
            {
                dialogResult = MessageBox.Show("You loose" + Environment.NewLine + " Do you want to start a new game?", "Confirmation", MessageBoxButtons.YesNoCancel);
                //MessageBox.Show("You loose \t Do you want to start a new game?", "Form1", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);

                if (dialogResult == DialogResult.Yes)
                {
                    newGame();
                }

                else if (dialogResult == DialogResult.No)
                {
                    Close();
                }
            }



        }

        public void Shuffle<T>(List<T> list)
        {
            Random rng = new Random();

            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        private void newGame()
        {
            allLocations.Remove(textToButton[""].Location);

            Shuffle(allLocations);

            allLocations.Add(textToButton[""].Location);

            int index = 0;

            foreach (Control c in this.Controls)
            {

                if (c is Button)
                {
                    Button b = (Button)c;

                    if (b.Text.ToString() == "")
                    {
                        b.Location = (Point)textToButton[""].Tag;
                        b.Visible = false;
                        continue;
                    }
                    b.Location = allLocations[index++];

                    Color randomColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                    b.BackColor = randomColor;
                }
            }

            return;
        }

        private Button getButtonInPoint(Point p)
        {
            foreach (Control c in this.Controls)
            {
                if (c is Button)
                {
                    Button b = (Button)c;
                    if (b.Location.ToString() == p.ToString())
                    {
                        return b;
                    }
                }
            }

            return null;
        }

        private void newGameToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            newGame();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (isInMovement)
            {
                return;
            }

            Button b = sender as Button;

            if (b == null)
            {
                Console.WriteLine("sender is not a button");
                return;
            }

            if (b.Text.ToString() == "")
            {
                Console.WriteLine("ignore, click on empty cell");
                return;
            }

            List<Point> neighbors = pointToNeighborsLocation[b.Location];

            foreach (Point p in neighbors)
            {
                Button bb= getButtonInPoint(p);

                if(bb == null)
                {
                    Console.WriteLine("the empty cell is not a neighbor");
                    return;
                }

                if (bb.Text.ToString() == "")
                {
                    moveButton(b, bb);
                }
            }
            afterTurn();
        }

        private void moveButton(Button toBeMoved, Button empty)
        {

            Console.WriteLine("move function");
            //MessageBox.Show("move function");
            buttonToBeMoved = toBeMoved;
            origin = buttonToBeMoved.Location;
            destination = empty.Location;
            isInMovement = true;

            this.timer1.Enabled = true;

            empty.Location = origin;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!isInMovement)
            {
                return;
            }
            if (buttonToBeMoved.Location.X != destination.X)
            {
                int x = (buttonToBeMoved.Location.X < destination.X) ?
                        buttonToBeMoved.Location.X + 1 :
                        buttonToBeMoved.Location.X - 1;

                origin.X = x;
            }
            else if (buttonToBeMoved.Location.Y != destination.Y)
            {
                int y = (buttonToBeMoved.Location.Y < destination.Y) ?
                        buttonToBeMoved.Location.Y + 1 :
                        buttonToBeMoved.Location.Y - 1;

                origin.Y = y;
            }
            else
            {
                isInMovement = false;
                this.timer1.Enabled = false;
            }

            buttonToBeMoved.Location = origin;
        }
    }
    
}
