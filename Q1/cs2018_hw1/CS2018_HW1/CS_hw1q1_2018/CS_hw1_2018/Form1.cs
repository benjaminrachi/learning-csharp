﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CS_hw1_2018
{
    public partial class Form1 : Form
    {
        int[] numbers = new int[15];
        Button[] buttons = new Button[16];
        const int rows = 4;
        const int columns = 4;
        Random c = new Random();
        Random rnd = new Random();
        bool inMotion = false;
        public Form1()
        {
            InitializeComponent();

            MenuStrip MnuStrip = new MenuStrip();
            ToolStripMenuItem MnuStripItem = new ToolStripMenuItem( );
            MnuStripItem.Text = "New Game";
            MnuStripItem.Name = "New Game";
            MnuStripItem.Click += newGameToolStripMenuItem_Click;
            MnuStrip.Items.Add(MnuStripItem);
            this.Controls.Add(MnuStrip);

            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = i + 1;
            }
            newGame();
        }
       
        private void newGame()
        {
            //buttons = new Button[16];
            removeAllButtons();
            drawingTable(16);
        }

        private void removeAllButtons()
        {
            //to remove all buttons
            foreach(Button button in buttons)
            {
                this.Controls.Remove(button);
            }
            
        }

        private void drawingTable(int n)
        {
            Shuffle(numbers);

            //initial location
            int x = 35;
            int y = 35;

            for (int i = 0; i < n; i++)
            {
                if (i % rows == 0 && i != 0) //go to the next line
                {
                    x = 35;
                    y += 52;
                }
                Button b = new Button();
                b.Location = new Point(x, y);
                x += 55;
                b.Height = 50;
                b.Width = 50;
                b.BackColor = Color.FromArgb(c.Next(0, 255), c.Next(0, 255), c.Next(0, 255), c.Next(0, 255));
                b.Tag = i.ToString();
                b.Click += buttonsClick;

                if (i == n - 1)
                {
                    b.Text = "";
                    b.Visible = false;
                }
                else
                {
                    b.Text = numbers[i].ToString();
                    b.Visible = true;
                }

                Controls.Add(b);
                buttons[i] = b;
            }
        }

        private void buttonsClick(object sender, EventArgs e)
        {
            Button origin = sender as Button;

            if ((inMotion == true) || (origin.Text.ToString().Equals("")))
            {
                return;
            }

            int idx = Int32.Parse(origin.Tag.ToString());

            if (idx < 12)
            {
                //check bottom
                checkBottom(origin, idx);
            }
            if (idx > 3)
            {
                //check up
                checkUp(origin, idx);
            }
            if (idx % 4 != 0)
            {
                //check left
                checkLeft(origin, idx);
            }
            if ((idx + 1) % 4 != 0)
            {
                //check right
                checkRight(origin, idx);
            }
            afterTurn();
            return;

        }

        private void checkRight(Button origin, int idx)
        {
            if (buttons[idx + 1].Text.ToString().Equals(""))
            {
                swapButtons(origin, buttons[idx + 1]);
            }
            return;
        }

        private void checkLeft(Button origin, int idx)
        {
            if (buttons[idx - 1].Text.ToString().Equals(""))
            {
                swapButtons(origin, buttons[idx - 1]);
            }
            return;
        }

        private void checkUp(Button origin, int idx)
        {
            if (buttons[idx - 4].Text.ToString().Equals(""))
            {
                swapButtons(origin, buttons[idx - 4]);
            }
            return;
        }

        private void checkBottom(Button origin, int idx)
        {
            if (buttons[idx + 4].Visible == false)
            {
                swapButtons(origin, buttons[idx + 4]);
            }
            return;
        }

        private void Shuffle(int[] numbers)
        {
            int n = numbers.Length;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                int value = numbers[k];
                numbers[k] = numbers[n];
                numbers[n] = value;
            }
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Start new game?", "New Game", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                newGame();
            }
        }

        private void swapButtons(Button original, Button empty)
        {
            original.Visible = false;
            empty.Visible = true;

            empty.Text = original.Text;
            original.Text = "";

            empty.BackColor = original.BackColor;
        }


        private bool didWin()
        {
            for (int i = 0; i < buttons.Length-1; i++)
            {
                if (! buttons[i].Text.ToString().Equals(i+1.ToString()))
                {
                    return false;
                }
            }

            return true;
        }

        private bool didLose()
        {
            bool first = false;
            bool second = false;
            for (int i = 0; i < 13; i++)
            {
                if (!buttons[i].Text.ToString().Equals(i + 1.ToString()))
                {
                    first= false;
                }
            }
            first =  true;
            if(buttons[13].Text.ToString().Equals(15.ToString()) && buttons[14].Text.ToString().Equals(14.ToString()) && buttons[15].Text.ToString().Equals(""))
            {
                second = true;
            }
            return first && second;
        }

        private void afterTurn()
        {

            if (didWin())
            {
                MessageBox.Show("Victory! :)", "Win", MessageBoxButtons.OK, MessageBoxIcon.Information);

                var result = MessageBox.Show("Start new game?", "New Game", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    newGame();
                }
            }

            else if (didLose())
            {
                MessageBox.Show("Arrrr! you lost", "Lose", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                var result = MessageBox.Show("Start new game?", "New Game", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    newGame();
                }
            }
        }
    }
}
