﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colors_Transports_Sort_UserControls
{
    public partial class UserControl1 : UserControl
    {
        public Control[] arrControls;
        private static Random myRand = new Random();

        public UserControl1(int n, int r, string fullEmpty)
        {
            InitializeComponent();
            arrControls = new Control[n * r];
            this.Width = n * 41 + 7;
            this.Height = r * 31 + 7;
            if( fullEmpty ==  "Empty")
                return;
            for (int j = 0; j < r; j++)
                for (int i = 0; i < n; i++)
                {
                    Control temp;
                    if(myRand.Next(2) == 0)
                    {    temp = new Label();
                         ((Label)temp).TextAlign = ContentAlignment.MiddleCenter;
                    }
                    else
                        temp = new Button();
                    temp.Size = new Size(40, 30);
                    switch (myRand.Next(4))
                    {
                        case 0: temp.BackColor = Color.Red; break;
                        case 1: temp.BackColor = Color.Green; break;
                        case 2: temp.BackColor = Color.Blue; break;
                        case 3: temp.BackColor = Color.Yellow; break;
                    }
                    temp.Text = myRand.Next(1, 100).ToString();
                    temp.Font = new Font("Arial", 12);
                    temp.ForeColor = Color.FromArgb(255 - temp.BackColor.R, 255 - temp.BackColor.G, 255 - temp.BackColor.B);

                    temp.Location = new Point(3 + 41 * i, 3 + 31 * j);
                    temp.TabIndex = i + j * n;
                    this.arrControls[i + j * n] = temp;
                    this.Controls.Add(temp);
                }
        }
    }
}
