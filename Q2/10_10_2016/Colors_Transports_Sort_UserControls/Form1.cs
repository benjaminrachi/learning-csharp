﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colors_Transports_Sort_UserControls
{
    public delegate void MyAddDelegate(UserControl1 ucTo, int counterTo, UserControl1 ucFrom, int indexFrom);
    public delegate void MyRemoveDelegate(UserControl1 uc, int i);

    public partial class Form1 : Form
    {
        private const int N = 50, Row = 2, NTransport = 5, NTo = 30;
        private Color[] _arrColors = new Color[] { Color.Red, Color.Green, Color.Blue, Color.Yellow };
        private Random _random = new Random();

        private readonly UserControl1 _arrUcFrom;
        private readonly UserControl1[] _arrUcTransport = new UserControl1[2];
        private readonly UserControl1[] _arrUcTo = new UserControl1[2];

        private readonly int[] _arrCounterTransport = new int[2];
        private readonly int[] _arrCounterDestination = new int[2];

        private readonly int[] _arrButtonLabelFlag = new int[4];  // 0-button, 1-label, 2-end 
        private readonly int[] _arrColorIndexFlag = { 0, 2 };   // 0-red, 1-green, 2-blue, 3-yellow
        private readonly bool[] _arrColorIsEnd = { false, false, false, false };

        private readonly Thread[] _threadToTransport = new Thread[2];
        private readonly Thread[] _threadToDestination = new Thread[2];

        private readonly AutoResetEvent[] _resetToTransport = new AutoResetEvent[2];
        private readonly AutoResetEvent[] _resetToDestination = new AutoResetEvent[2];

        private SortedList<double, Control> buttonsRed = new SortedList<double, Control>();
        private SortedList<double, Control> buttonsGreen = new SortedList<double, Control>();
        private SortedList<double, Control> buttonsBlue = new SortedList<double, Control>();
        private SortedList<double, Control> buttonsYellow = new SortedList<double, Control>();
        private SortedList<double, Control> labelsRed = new SortedList<double, Control>();
        private SortedList<double, Control> labelsGreen = new SortedList<double, Control>();
        private SortedList<double, Control> labelsBlue = new SortedList<double, Control>();
        private SortedList<double, Control> labelsYellow = new SortedList<double, Control>();



        public Form1()
        {
            InitializeComponent();
            _arrUcFrom = new UserControl1(N / 2, 2, "Full");
            _arrUcFrom.Location = new Point(2, 50);
            this.Controls.Add(_arrUcFrom);
            ArrUcToSortedLIst();

            for (int i = 0; i < 2; i++)
            {
                _arrUcTransport[i] = new UserControl1(NTransport, 1, "Empty");
                _arrUcTransport[i].Location = new Point(2 + 340 * i, 153);
                this.Controls.Add(_arrUcTransport[i]);

                _arrUcTo[i] = new UserControl1(NTo, 1, "Empty");
                _arrUcTo[i].Location = new Point(2, 217 + 72 * i);
                this.Controls.Add(_arrUcTo[i]);
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (var i = 0; i < 2; i++)
            {
                _threadToTransport[i] = new Thread(function_toTransport);
                _threadToDestination[i] = new Thread(function_toDestination);

                _resetToTransport[i] = new AutoResetEvent(false);
                _resetToDestination[i] = new AutoResetEvent(false);

                _threadToTransport[i].Start(i);
                _threadToDestination[i].Start(i);
            }
        }

        private void function_toTransport(object obj)
        {
            var indexColor = (int)obj;

            for (int i = 0; i < N; i++)
            {
                if (_arrUcFrom.arrControls[i] == null) continue;

                //var temp = _arrUcFrom.arrControls[i];
                var pos = indexColor == 0 ? 0 : 1;

                var minIndex = GetMinIndex(_arrColorIndexFlag[pos], _arrButtonLabelFlag[_arrColorIndexFlag[pos]]);

                //no compatible control found --> change the query or stop if you finish all
                if (minIndex == -1)
                {
                    //change the type query
                    if (_arrButtonLabelFlag[_arrColorIndexFlag[pos]] == 0)
                    { _arrButtonLabelFlag[_arrColorIndexFlag[pos]]++;
                        continue;

                    }
                    else
                    {
                        //change the color query
                        if (_arrColorIndexFlag[pos] == 0 || _arrColorIndexFlag[pos] == 2)
                        {
                            _arrColorIsEnd[_arrColorIndexFlag[pos]] = true;
                            _arrColorIndexFlag[pos]++;
                            continue;
                        }

                        //you finish
                        else
                        {
                            _arrColorIsEnd[_arrColorIndexFlag[pos]] = true;
                            break;
                        }
                    }
                }

                //compatible control found, add to the transport
                else
                {
                    Invoke(new MyAddDelegate(Add), _arrUcTransport[indexColor], _arrCounterTransport[indexColor], _arrUcFrom, minIndex);
                    Thread.Sleep(50);
                    _arrCounterTransport[indexColor]++;
                    Invoke(new MyRemoveDelegate(Remove), _arrUcFrom, minIndex);
                    Thread.Sleep(50);

                    if (_arrCounterTransport[indexColor] != NTransport) continue;
                    _resetToDestination[indexColor].Set();
                    _resetToTransport[indexColor].WaitOne();

                }

            }
            //_arrColorIsEnd[_arrColorIndexFlag[indexColor]] = true;
            _resetToDestination[indexColor].Set();
            _resetToTransport[indexColor].WaitOne();
        }

        private void function_toDestination(object obj)
        {
            var indexColor = (int)obj;
            while (true)
            {
                _resetToDestination[indexColor].WaitOne();

                for (var i = 0; i < _arrCounterTransport[indexColor]; i++)
                {
                    var temp = _arrUcTransport[indexColor].arrControls[i];
                    if (temp == null) continue;
                    Invoke(new MyAddDelegate(Add), _arrUcTo[indexColor], _arrCounterDestination[indexColor], _arrUcTransport[indexColor], i);
                    Thread.Sleep(50);
                    _arrCounterDestination[indexColor]++;
                    Invoke(new MyRemoveDelegate(Remove), _arrUcTransport[indexColor], i);
                    Thread.Sleep(50);
                }
                _arrCounterTransport[indexColor] = 0;
                if (_arrColorIsEnd[indexColor] == false)
                    _resetToTransport[indexColor].Set();
                else
                    break;
            }
        }


        private int GetMinIndex(int colorFlag, int buttonLabelFlag)
        {
            int result = -1;

            try
            {
                switch (colorFlag)
                {
                    case 0: //red
                        switch (buttonLabelFlag)
                        {
                            case 0: result = buttonsRed.First().Value.TabIndex; buttonsRed.RemoveAt(0); break;
                            case 1: result = labelsRed.First().Value.TabIndex; labelsRed.RemoveAt(0); break;

                        }
                        break;

                    case 1: //green
                        switch (buttonLabelFlag)
                        {
                            case 0: result = buttonsGreen.First().Value.TabIndex; buttonsGreen.RemoveAt(0); break;
                            case 1: result = labelsGreen.First().Value.TabIndex; labelsGreen.RemoveAt(0); break;

                        }
                        break;

                    case 2: //blue
                        switch (buttonLabelFlag)
                        {
                            case 0: result = buttonsBlue.First().Value.TabIndex; buttonsBlue.RemoveAt(0); break;
                            case 1: result = labelsBlue.First().Value.TabIndex; labelsBlue.RemoveAt(0); break;

                        }
                        break;

                    case 3: //yellow
                        switch (buttonLabelFlag)
                        {
                            case 0: result = buttonsYellow.First().Value.TabIndex; buttonsYellow.RemoveAt(0); break;
                            case 1: result = labelsYellow.First().Value.TabIndex; labelsYellow.RemoveAt(0); break;

                        }
                        break;

                    default:
                        return -1;
                }
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return -1;
            }


        }

        private void ArrUcToSortedLIst()
        {
            foreach (var c in _arrUcFrom.arrControls)
            {
                if (c.BackColor == Color.Red && c.GetType().Name == "Button") buttonsRed.Add((double)(Int32.Parse(c.Text)* _random.Next(100)*0.001 ), c);
                else if (c.BackColor == Color.Red && c.GetType().Name == "Label") labelsRed.Add((double)(Int32.Parse(c.Text) * _random.Next(100) * 0.001), c);

                else if (c.BackColor == Color.Green && c.GetType().Name == "Button") buttonsGreen.Add((double)(Int32.Parse(c.Text) * _random.Next(100) * 0.001), c);
                else if (c.BackColor == Color.Green && c.GetType().Name == "Label") labelsGreen.Add((double)(Int32.Parse(c.Text) * _random.Next(100) * 0.001), c);

                else if (c.BackColor == Color.Blue && c.GetType().Name == "Button") buttonsBlue.Add((double)(Int32.Parse(c.Text) * _random.Next(100) * 0.001), c);
                else if (c.BackColor == Color.Blue && c.GetType().Name == "Label") labelsBlue.Add((double)(Int32.Parse(c.Text) * _random.Next(100) * 0.001), c);

                else if (c.BackColor.Name == "Yellow" && c.GetType().Name == "Button") buttonsYellow.Add((double)(Int32.Parse(c.Text) * _random.Next(100) * 0.001), c);
                else if (c.BackColor.Name == "Yellow" && c.GetType().Name == "Label") labelsYellow.Add((double)(Int32.Parse(c.Text) * _random.Next(100) * 0.001), c);
            }

        }

        private static void Add(UserControl1 ucTo, int counterTo, UserControl1 ucFrom, int indexFrom)
        {
            ucTo.arrControls[counterTo] = ucFrom.arrControls[indexFrom];
            ucTo.Controls.Add(ucFrom.arrControls[indexFrom]);
            ucTo.arrControls[counterTo].Location = new Point(2 + 21 * counterTo, 3);
        }

        private static void Remove(UserControl1 uc, int index)
        {
            uc.Controls.Remove(uc.arrControls[index]);
            uc.arrControls[index] = null;
        }
    }
}
