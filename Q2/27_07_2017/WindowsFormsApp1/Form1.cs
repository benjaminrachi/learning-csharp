﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public delegate void myAddDelegate(UserControl1 UC_To, int counter_To, UserControl1 UC_From, int index_From);
    public delegate void myRemoveDelegate(UserControl1 UC, int i);
    public partial class Form1 : Form
    {
        private const int Total = 52; 

        private UserControl1[]  arrUC_From = new UserControl1[3],
                                arrUC_To = new UserControl1[3],
                                arrUC_Transport = new UserControl1[3];

        private int[]   arrCounter_From = new int[3], //
                        arrCounter_Transport = new int[3],
                        arrCounter_To = new int[3];

        private bool[] arrIsEnd = new bool[3];
        
        private Color[] arrColors = new Color[] { Color.Red, Color.Green, Color.Blue };

        private Thread[] arr_toTransport = new Thread[3], arr_fromTransport = new Thread[3];

        private AutoResetEvent[] toTransport = new AutoResetEvent[3], fromTransport = new AutoResetEvent[3];
        //fromTransport = from the TRANSPORT to the TO =AutoResetEvent_1
        //toTransport = from the FROM to the TRANSPORT =AutoResetEvent_2

        public Form1()
        {
            InitializeComponent();

            for (int i = 0; i < 3; i++)
            {
                arrUC_From[i] = new UserControl1(Total, "Full", i);
                arrUC_From[i].Location = new Point(2, 40 + 55 * i);
                this.Controls.Add(arrUC_From[i]);

                arrUC_Transport[i] = new UserControl1(5, "Empty", 0);
                arrUC_Transport[i].Location = new Point(2 + 135 * i, 210);
                this.Controls.Add(arrUC_Transport[i]);

                arrUC_To[i] = new UserControl1(Total, "Empty", 0);
                arrUC_To[i].Location = new Point(2, 270 + 55 * i);
                this.Controls.Add(arrUC_To[i]);

                fromTransport[i] = new AutoResetEvent(false);
                toTransport[i] = new AutoResetEvent(false);
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 3; i++)
            {
                arr_toTransport[i] = new Thread(toTransport_Function);
                arr_fromTransport[i] = new Thread(fromTransport_Function);

                arr_toTransport[i].Start(i);
                arr_fromTransport[i].Start(i);
            }
        }

        private void toTransport_Function(object obj)
        {
            int indexColor = (int)obj;

            for (int k = 0; k < 3; k++)
            {
                //ignoring the array where the color is absent
                if (indexColor == k)
                {
                    arrCounter_From[indexColor]++;
                    continue;
                }

                for (int i = 0; i < Total; i++)
                {
                    Label temp = arrUC_From[k].arrLabels[i];
                    if (temp == null || temp.BackColor == Color.White)
                        continue;

                    if (indexColor == 0 && temp.BackColor.R != 0 || indexColor == 1 && temp.BackColor.G != 0 || indexColor == 2 && temp.BackColor.B != 0)
                    {
                        this.Invoke(new myAddDelegate(add), arrUC_Transport[indexColor], arrCounter_Transport[indexColor], arrUC_From[k], i);
                        Thread.Sleep(50);
                        this.Invoke(new myRemoveDelegate(remove), arrUC_From[k], i);
                        arrCounter_Transport[indexColor]++;
                        Thread.Sleep(50);

                        if (arrCounter_Transport[indexColor] == 5)
                        {
                            fromTransport[indexColor].Set();
                            toTransport[indexColor].WaitOne();
                        }
                    }
                }
                arrCounter_From[indexColor]++;
                arrIsEnd[indexColor] = arrCounter_From[indexColor] == 3 ?  true : false;


            }
            fromTransport[indexColor].Set();
        }

        private void add(UserControl1 UC_To, int counter_To, UserControl1 UC_From, int index_From)
        {
            UC_To.arrLabels[counter_To].BackColor = UC_From.arrLabels[index_From].BackColor;
        }

        private void remove(UserControl1 UC, int index)
        {
            UC.arrLabels[index].BackColor = Color.White;
        }

        private void fromTransport_Function(object o)
        {
            int indexColor = (int)o;
            while (true)
            {
                fromTransport[indexColor].WaitOne();
                for (int i = 0; i < arrCounter_Transport[indexColor]; i++)
                {
                    this.Invoke(new myAddDelegate(add), arrUC_To[indexColor], arrCounter_To[indexColor], arrUC_Transport[indexColor], i);
                    Thread.Sleep(50);
                    arrCounter_To[indexColor]++;
                    this.Invoke(new myRemoveDelegate(remove), arrUC_Transport[indexColor], i);
                    Thread.Sleep(50);
                   
                }
                if (arrIsEnd[indexColor] != true)
                {
                    arrCounter_Transport[indexColor] = 0;
                    toTransport[indexColor].Set();
                }
                else
                    break;
            }
        }
    }
}
