﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Threading;

namespace Red_Blue_Transport_UserControls
{
    public partial class Form1 : Form
    {
        private UserControl1[] 
            arrUC_Storage = new UserControl1[2], 
            arrUC_Transport = new UserControl1[2];
        private const int N = 52;

        public Form1()
        {
            InitializeComponent();

            for (int i = 0; i < 2; i++)
            {
                arrUC_Storage[i] = new UserControl1(N, "Full");
                arrUC_Storage[i].Location = new Point(2, 49 + 140 * i);
                this.Controls.Add(arrUC_Storage[i]);

                arrUC_Transport[i] = new UserControl1(5, "Empty");
                arrUC_Transport[i].Location = new Point(2 + 218 * i, 120);
                this.Controls.Add(arrUC_Transport[i]);
            }
        }

        private Thread[]
            arr_toTransport = new Thread[3],
            arr_fromTransport = new Thread[3];

        private bool started = false;

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (started) return;
            started = true;

            arr_toTransport[0] = new Thread(moveToTransport);
            arr_fromTransport[0] = new Thread(moveFromTransport);

            arr_toTransport[1] = new Thread(moveToTransport);
            arr_fromTransport[1] = new Thread(moveFromTransport);

            arr_toTransport[0].Start(0);
            arr_fromTransport[0].Start(0);

            arr_toTransport[1].Start(1);
            arr_fromTransport[1].Start(1);
        }

        public delegate void addItemFromUC(UserControl1 toUc, Color color);
        public delegate void removeItemFromUC(UserControl1 uc, int index);

        private AutoResetEvent[] arrAutoResetEvent_1 = new AutoResetEvent[] { new AutoResetEvent(false), new AutoResetEvent(false) };
        private AutoResetEvent[] arrAutoResetEvent_2 = new AutoResetEvent[] { new AutoResetEvent(false), new AutoResetEvent(false) };

        private bool[] didFinish = new bool[] { false, false };

        private int[] arrCounter_To = new int[] { 0, 0 };
        private int[] arrCounter_Transport = new int[] { 0, 0 };

        private void moveFromTransport(object id)
        {
            int colorId = (int)id;
            int TRANSPORT_CAPACITY = 5;

            var arrLabels = ((UserControl1)arrUC_Storage[colorId]).arrLabels;
            var parent = (UserControl1)arrUC_Storage[colorId];

            while (!didFinishLoading(colorId))
            {
                for (int i = 0; i < arrLabels.Length; i++)
                {
                    var item = arrLabels[i];

                    // ignore in null
                    if (item == null) continue;

                    // ignore white
                    if (item.BackColor == Color.White) continue;

                    // ignore if not the same color
                    if (!isSameColorAsThread(colorId, item)) continue;

                    this.Invoke(new addItemFromUC(addItem), arrUC_Transport[(colorId + 1) % 2], item.BackColor);
                    this.Invoke(new removeItemFromUC(removeItem), parent, i);

                    arrCounter_Transport[colorId]++;

                    Thread.Sleep(100);

                    if (arrCounter_Transport[colorId] == TRANSPORT_CAPACITY)
                    {
                        arrAutoResetEvent_1[colorId].Set();
                        arrAutoResetEvent_2[colorId].WaitOne();
                    }
                }
            }

            // release after finish
            arrAutoResetEvent_1[colorId].Set();
            didFinish[colorId] = true;
        }

        private bool didFinishLoading(int colorId)
        {
            var arrLabels = ((UserControl1)arrUC_Storage[colorId]).arrLabels;

            foreach (var item in arrLabels)
            {

                // ignore whites
                if (item.BackColor == Color.White)
                {
                    continue;
                }

                if (isSameColorAsThread(colorId, item))
                {
                    return false;
                }
            }

            return true;
        }

        private void moveToTransport(object id)
        {
            int colorId = (int)id;

            while (true)
            {
                arrAutoResetEvent_1[colorId].WaitOne();

                for (int i = 0; i < arrUC_Transport[colorId].arrLabels.Length; i++)
                {
                    var item = arrUC_Transport[colorId].arrLabels[i];

                    // skip nulls
                    if (item == null) continue;

                    // skip whites
                    if (item.BackColor == Color.White) continue;

                    this.Invoke(new addItemFromUC(addItem), arrUC_Storage[colorId], item.BackColor);
                    arrCounter_To[colorId]++;

                    this.Invoke(new removeItemFromUC(removeItem), arrUC_Transport[colorId], i);
                    arrCounter_Transport[colorId]--;

                    Thread.Sleep(100);
                }

                arrCounter_Transport[colorId] = 0;

                if (didFinish[colorId])
                {
                    break;
                }

                arrAutoResetEvent_2[colorId].Set();
            }
        }

        static private bool isSameColorAsThread(int threadId, Control c)
        {
            // red
            if (threadId == 0) return c.BackColor.B > 0;

            // blue
            if (threadId == 1) return c.BackColor.R > 0;

            return false;
        }

        private void addItem(UserControl1 toUc, Color color)
        {
            int index = findNextBlankIndexInUc(toUc);

            if (index > -1) 
                toUc.arrLabels[index].BackColor = color;
        }

        private int findNextBlankIndexInUc(UserControl1 uc)
        {
            for (int i = 0; i < uc.arrLabels.Length; i++)
            {
                if (uc.arrLabels[i].BackColor == Color.White)
                {
                    return i;
                }
            }

            return -1;
        }

        private void removeItem(UserControl1 uc, int index)
        {
            uc.arrLabels[index].BackColor = Color.White;
        }
    }
}
