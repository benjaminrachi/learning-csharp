﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Red_Blue_Transport_UserControls
{
    public partial class UserControl1 : UserControl
    {
        static Random myRand = new Random();
        public Label[] arrLabels;
        public UserControl1(int counter, string fullEmpty)
        {
            InitializeComponent();
            arrLabels = new Label[counter];
            this.Width = counter * 21 + 7;

            if (fullEmpty == "Empty")
            {
                for (int i = 0; i < counter; i++)
                {
                    arrLabels[i] = new Label();
                    arrLabels[i].Size = new Size(20, 30);
                    arrLabels[i].BackColor = Color.White;
                    arrLabels[i].Location = new Point(2 + 21 * i, 4);
                    this.Controls.Add(arrLabels[i]);
                }
            }
            else
            {
                for (int i = 0; i < counter; i++)
                {
                    arrLabels[i] = new Label();
                    arrLabels[i].Size = new Size(20, 30);
                    if (i < counter * 3/4)
                    {
                        if (myRand.Next(2) == 0)
                            arrLabels[i].BackColor = Color.Red;
                        else
                            arrLabels[i].BackColor = Color.Blue;
                    }
                    else
                        arrLabels[i].BackColor = Color.White;

                    arrLabels[i].Location = new Point(2 + 21 * i, 4);
                    this.Controls.Add(arrLabels[i]);
                }
            }
        }
    }
}
