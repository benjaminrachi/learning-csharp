﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace RGB_ButtonLabel_Transport_UserControl
{
    public delegate void MyAddDelegate(UserControl1 ucTo, int counterTo, UserControl1 ucFrom, int indexFrom);
    public delegate void MyRemoveDelegate(UserControl1 uc, int i);

    public partial class Form1 : Form
    {
        #region declarations
        private const int Total = 55;


        private readonly UserControl1[] _arrUcFrom = new UserControl1[2];
        private readonly UserControl1[] _arrUcTo = new UserControl1[3];
        private readonly UserControl1[] _arrUcTransport = new UserControl1[3];


        private readonly int[] _arrCounterTransport = new int[3];
        private readonly int[] _arrCounterDestination = new int[3];


        private readonly int[] _arrButtonLabelFlag = new int[3];  // 0 - button, 1 - label, 2 - end
        private readonly bool[] _arrColorIsEnd = { false, false, false };


        private readonly Thread[] _threadToTransport = new Thread[3];
        private readonly Thread[] _threadToDestination = new Thread[3];


        private readonly AutoResetEvent[] _resetToTransport = new AutoResetEvent[3];
        private readonly AutoResetEvent[] _resetToDestination = new AutoResetEvent[3];
#endregion

        public Form1()
        {
            InitializeComponent();

            _arrUcFrom[0] = new UserControl1(Total, "Full", "Button");
            _arrUcFrom[1] = new UserControl1(Total, "Full", "Label");

            for (int i = 0; i < 2; i++)
            {
                _arrUcFrom[i].Location = new Point(2, 40 + 55 * i);
                this.Controls.Add(_arrUcFrom[i]);
            }

            for (int i = 0; i < 3; i++)
            {
                _arrUcTransport[i] = new UserControl1(5, "Empty", "");
                _arrUcTransport[i].Location = new Point(2 + 135 * i, 155);
                this.Controls.Add(_arrUcTransport[i]);

                _arrUcTo[i] = new UserControl1(Total, "Empty", "");
                _arrUcTo[i].Location = new Point(2, 215 + 55 * i);
                this.Controls.Add(_arrUcTo[i]);
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (var i = 0; i < 3; i++)
            {
                _threadToTransport[i] = new Thread(function_toTransport);
                _threadToDestination[i] = new Thread(function_toDestination);

                _resetToTransport[i] = new AutoResetEvent(false);
                _resetToDestination[i] = new AutoResetEvent(false);

                _threadToTransport[i].Start(i);
                _threadToDestination[i].Start(i);
            }
        }

    
        private void function_toTransport(object obj)
        {
            var indexColor = (int)obj;

            for (var k = 0; k < 2; k++)
            {

                for (var i = 0; i < Total; i++)
                {
                    if (_arrUcFrom[k].arrControls[i] == null) continue;

                    var temp = _arrUcFrom[k].arrControls[i];
                    if (!IsValidType(temp, indexColor)) continue;

                    Invoke(new MyAddDelegate(Add), _arrUcTransport[indexColor], _arrCounterTransport[indexColor], _arrUcFrom[k], i);
                    Thread.Sleep(50);
                    _arrCounterTransport[indexColor]++;
                    Invoke(new MyRemoveDelegate(Remove), _arrUcFrom[k], i);
                    Thread.Sleep(50);

                    if (_arrCounterTransport[indexColor] != 5) continue;
                    _resetToDestination[indexColor].Set();
                    _resetToTransport[indexColor].WaitOne();
                }
                _arrButtonLabelFlag[indexColor]++;

                //finish the buttons and the labels
                if (_arrButtonLabelFlag[indexColor] == 2) break;

            }
            _arrColorIsEnd[indexColor] = true;
            _resetToDestination[indexColor].Set();

        }



        private void function_toDestination(object obj)
        {
            var indexColor = (int)obj;
            while (true)
            {
                _resetToDestination[indexColor].WaitOne();

                for (var i = 0; i < _arrCounterTransport[indexColor]; i++)
                {
                    var temp = _arrUcTransport[indexColor].arrControls[i];
                    if (temp == null) continue;
                    Invoke(new MyAddDelegate(Add), _arrUcTo[indexColor], _arrCounterDestination[indexColor],_arrUcTransport[indexColor], i);
                    Thread.Sleep(50);
                    _arrCounterDestination[indexColor]++;
                    Invoke(new MyRemoveDelegate(Remove), _arrUcTransport[indexColor], i);
                    Thread.Sleep(50);
                }
                _arrCounterTransport[indexColor] = 0;
                if (_arrColorIsEnd[indexColor] == false)
                    _resetToTransport[indexColor].Set();
                else
                    break;
            }
        }

        private bool IsValidType(Control c, int indexColor)
        {
            switch (indexColor)
            {
                case 0: //red
                    return c.BackColor.R != 0 &&
                           (c.GetType().Name == "Button" && _arrButtonLabelFlag[0] == 0 || c.GetType().Name == "Label" && _arrButtonLabelFlag[0] == 1);

                case 1: //green
                    return c.BackColor.G != 0 &&
                           (c.GetType().Name == "Button" && _arrButtonLabelFlag[1] == 0 || c.GetType().Name == "Label" && _arrButtonLabelFlag[1] == 1);

                case 2: //blue
                    return c.BackColor.B != 0 &&
                           (c.GetType().Name == "Button" && _arrButtonLabelFlag[2] == 0 || c.GetType().Name == "Label" && _arrButtonLabelFlag[2] == 1);

                default:
                    return false;
            }

        }

        private static void Add(UserControl1 ucTo, int counterTo, UserControl1 ucFrom, int indexFrom)
        {
            ucTo.arrControls[counterTo] = ucFrom.arrControls[indexFrom];
            ucTo.Controls.Add(ucFrom.arrControls[indexFrom]);
            ucTo.arrControls[counterTo].Location = new Point(2 + 21 * counterTo, 3);
        }

        private static void Remove(UserControl1 uc, int index)
        {
            uc.Controls.Remove(uc.arrControls[index]);
            uc.arrControls[index] = null;
        }
    }
}
